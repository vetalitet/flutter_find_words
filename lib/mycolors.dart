import 'package:flutter/material.dart' hide Colors;

class MyColors {
  static const Color splash_color_1 = const Color(0xFFf46b5b);
  static const Color splash_color_2 = const Color(0xFFffcc23);

  static const Color menuHeader = const Color(0xFF35A4A5);
  static const Color mainScreenHeader = const Color(0xFF2aca96);
  static const Color buyCoinsButtonPurple = const Color(0xFF862d63);
  static const Color buyCoinsButtonPurpleDark = const Color(0xFF701a4e);
  static const Color buyCoinsButtonDisabledGray = const Color(0xFF777777);

  static const Color bgPaint = const Color(0x00F0F0F0);
  static const Color itemPaint = const Color(0xFFFFFFFF);
  static const Color itemPaintCompleted = const Color(0xFFEFF3FF);
  static const Color borderPaint = const Color(0xFF000000);
  static const Color textPaint = const Color(0xD316324C);

  static const Color bgColorTop = const Color(0xFF33D2D1);
  static const Color bgColorBottom = const Color(0xFF004484);

  static const Color levelBg = const Color(0xFF85E7B4);

  static const Color buttonLightPurple = const Color(0xFF862d63);
  static const Color buttonBlue = const Color(0xFF1e5d97);
  static const Color lockedBg = const Color(0xFFd9dfed);
  static const Color lockedGrayedBg = const Color(0xB2FFFFFF);

  static const Color dialogTextColor = const Color(0xFF2C3F67);
  static const Color dialogDelimiterColor = const Color(0xFFbbc3d5);
  static const Color defaultTextColor = const Color(0xFF000000);
  static const Color freeButtonColor = const Color(0xFF059ffa);
  static const Color bottomAreaColor = const Color(0xFFd9dfed);
}
