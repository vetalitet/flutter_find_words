import 'package:findwords/components/buttons/buy_coins_button.dart';
import 'package:findwords/mycolors.dart';
import 'package:findwords/shared_preferences/shared_preferences_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ToolbarWallet extends StatelessWidget {
  final VoidCallback onPress;

  ToolbarWallet({Key key, @required this.onPress}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
      width: double.infinity,
      height: 70.0,
      color: MyColors.menuHeader,
      child: Row(
        children: <Widget>[
          InkWell(
            child: Container(
              width: 20,
              child: SvgPicture.asset('images/back_arrow.svg'),
              margin: EdgeInsets.only(left: 20, right: 10),
            ),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
          Container(
            child: SvgPicture.asset(
              'images/wallet.svg',
              width: 30,
              height: 30,
            ),
            margin: EdgeInsets.only(left: 8.0, right: 8.0),
          ),
          Text(
            "Your coins",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
          Spacer(),
          Container(
            child: BuyCoinsButton(
                coins: SharedPreferenceManager.getCoins(),
                onPress: () {
                  if (onPress != null) {
                    onPress.call();
                  }
                }),
            margin: EdgeInsets.only(right: 24.0),
          )
        ],
      ),
    ));
  }
}
