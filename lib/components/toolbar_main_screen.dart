import 'package:findwords/components/buttons/buy_coins_button.dart';
import 'package:findwords/data/json/settings.dart';
import 'package:findwords/mycolors.dart';
import 'package:findwords/shared_preferences/shared_preferences_manager.dart';
import 'package:findwords/utils/calculate_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ToolbarMainScreen extends StatelessWidget {
  final String levelName;
  final int levelIndex;
  final int totalLevelCount;
  final int wordsCount;
  final VoidCallback backPress;
  final VoidCallback onPress;

  ToolbarMainScreen({
    Key key,
    @required this.levelName,
    @required this.levelIndex,
    @required this.totalLevelCount,
    @required this.wordsCount,
    @required this.backPress,
    @required this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double margin = getArrowMargin(context);
    Settings settings = SharedPreferenceManager.getSettings();
    String levelIndexString =
        settings.isAdditionalInfoShown ? " ($levelIndex/$totalLevelCount)" : "";
    double bottomPadding = settings.isAdditionalInfoShown ? 6.0 : 0.0;
    return SafeArea(
        child: Container(
      width: double.infinity,
      height: 70.0,
      color: MyColors.mainScreenHeader,
      child: Row(
        children: <Widget>[
          InkWell(
            child: Container(
              width: 20,
              child: SvgPicture.asset('images/back_arrow.svg'),
              margin: EdgeInsets.only(left: margin, right: margin),
            ),
            onTap: () {
              if (backPress != null) {
                backPress.call();
              }
              //Navigator.of(context).pop();
            },
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: bottomPadding),
                child: Text(
                  "$levelName$levelIndexString",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Visibility(
                visible: settings.isAdditionalInfoShown,
                child: Text(
                  "Words found: $wordsCount",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 14.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          Spacer(),
          Container(
            child: BuyCoinsButton(
                coins: SharedPreferenceManager.getCoins(),
                onPress: () {
                  if (onPress != null) {
                    onPress.call();
                  }
                }),
            margin: EdgeInsets.only(right: 24.0),
          )
        ],
      ),
    ));
  }
}
