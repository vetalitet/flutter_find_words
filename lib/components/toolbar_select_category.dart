import 'package:findwords/mycolors.dart';
import 'package:findwords/utils/calculate_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ToolbarSelectCategory extends StatelessWidget {
  final String title;
  final VoidCallback onPress;

  ToolbarSelectCategory({
    Key key,
    @required this.title,
    @required this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double margin = getArrowMargin(context);
    return SafeArea(
        child: Container(
      width: double.infinity,
      height: 70.0,
      color: MyColors.menuHeader,
      child: Row(
        children: <Widget>[
          InkWell(
            child: Container(
              width: 20,
              child: SvgPicture.asset('images/back_arrow.svg'),
              margin: EdgeInsets.only(left: margin, right: margin),
            ),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
          Text(
            title,
            style: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
                fontWeight: FontWeight.bold),
          ),
        ],
      ),
    ));
  }
}
