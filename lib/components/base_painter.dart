import 'package:findwords/data/enums/rect_status.dart';
import 'package:findwords/data/item_size_rect_items.dart';
import 'package:findwords/data/rect_item.dart';
import 'package:findwords/mycolors.dart';
import 'package:flutter/material.dart';

class BasePainter extends CustomPainter {
  Paint bgPaint;
  Paint itemPaint;
  Paint itemIsSelectingPaint;
  Paint borderPaint;
  Paint textPaint;
  Paint gridLinePaint;

  ItemSizeRectItems itemSizeRectItems;
  BasePainter(this.itemSizeRectItems) {
    initPaints();
  }

  void initPaints() {
    bgPaint = _getPaint(color: MyColors.bgPaint);
    itemPaint = _getPaint(color: MyColors.itemPaint);
    itemIsSelectingPaint = _getPaint(color: MyColors.itemPaint);

    borderPaint = _getPaint(color: MyColors.borderPaint);
    borderPaint.strokeWidth = 1.5;
    borderPaint.style = PaintingStyle.stroke;

    textPaint = _getPaint(color: MyColors.textPaint);
    gridLinePaint = _getPaint(color: Colors.white);
    gridLinePaint.strokeWidth = 1.5;
  }

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawRect(Rect.fromLTRB(0, 0, size.width, size.height), bgPaint);

    itemSizeRectItems.horizontalPoints.forEach((e) {
      canvas.drawLine(Offset(e.item1.x.toDouble(), e.item1.y.toDouble()),
          Offset(e.item2.x.toDouble(), e.item2.y.toDouble()), gridLinePaint);
    });

    itemSizeRectItems.verticalPoints.forEach((e) {
      canvas.drawLine(Offset(e.item1.x.toDouble(), e.item1.y.toDouble()),
          Offset(e.item2.x.toDouble(), e.item2.y.toDouble()), gridLinePaint);
    });

    for (RectItem rectItem in itemSizeRectItems.rectItems) {
      itemIsSelectingPaint.color = Color(rectItem.color);
      if (rectItem.status == RectStatus.NONE) {
        canvas.drawRRect(
            RRect.fromRectAndRadius(rectItem.rect, Radius.circular(6.0)),
            itemPaint);
      } else if (rectItem.status == RectStatus.SELECTED) {
        canvas.drawRRect(
            RRect.fromRectAndRadius(rectItem.rect, Radius.circular(6.0)),
            itemIsSelectingPaint);
      } else if (rectItem.status == RectStatus.IS_SELECTING) {
        canvas.drawRRect(
            RRect.fromRectAndRadius(rectItem.rect, Radius.circular(6.0)),
            itemIsSelectingPaint);
      }
      canvas.drawRRect(
          RRect.fromRectAndRadius(rectItem.rect, Radius.circular(6.0)),
          borderPaint);

      double itemSize = itemSizeRectItems.itemSize;

      _drawTextAt(
          rectItem.char,
          Offset(rectItem.rect.left + itemSize / 2,
              rectItem.rect.top + itemSize / 2),
          canvas);
    }
  }

  @override
  bool shouldRepaint(BasePainter oldDelegate) {
    return true;
  }

  Paint _getPaint({@required Color color}) => Paint()..color = color;

  void measureItems(Size size) {}

  void _drawTextAt(String text, Offset position, Canvas canvas) {
    int gridSize = itemSizeRectItems.rectItems.length;
    final textStyle = TextStyle(
      color: Colors.black,
      fontSize: getFontSize(gridSize != null ? gridSize : 30),
    );
    final textSpan = TextSpan(
      text: text,
      style: textStyle,
    );
    final textPainter = TextPainter(
        text: textSpan,
        textDirection: TextDirection.ltr,
        textAlign: TextAlign.center);
    textPainter.layout(minWidth: 0, maxWidth: 0);
    Offset drawPosition =
        Offset(position.dx, position.dy - (textPainter.height / 2));
    textPainter.paint(canvas, drawPosition);
  }

  double getFontSize(int gridSize) {
    double fontSize;
    if (gridSize == 25) {
      fontSize = 30.0;
    } else if (gridSize == 36) {
      fontSize = 28.0;
    } else {
      fontSize = 26.0;
    }
    return fontSize;
  }
}
