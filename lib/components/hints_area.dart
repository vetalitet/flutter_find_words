import 'package:findwords/coins.dart';
import 'package:findwords/components/buttons/hint_button.dart';
import 'package:findwords/custom/icons/find_words_crossword_icons.dart';
import 'package:findwords/mycolors.dart';
import 'package:flutter/cupertino.dart';

class HintArea extends StatefulWidget {
  final double width;
  final VoidCallback lowHintCallback;
  final VoidCallback highHintCallback;

  HintArea(
      {Key key,
      @required this.width,
      this.lowHintCallback,
      this.highHintCallback})
      : super(key: key);

  @override
  _HintAreaState createState() => _HintAreaState();
}

class _HintAreaState extends State<HintArea> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    color: MyColors.buttonBlue,
                    height: 1,
                  )),
              Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                child: Text(
                  "Hints",
                  style: TextStyle(
                    color: MyColors.buttonBlue,
                    fontSize: 18,
                  ),
                ),
              ),
              Expanded(
                  flex: 1,
                  child: Container(
                    color: MyColors.buttonBlue,
                    height: 1,
                  )),
            ],
          ),
          Padding(padding: EdgeInsets.only(bottom: 4)),
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: HintButton(
                      icon: FindWordsCrossword.low_m,
                      quantity: Coins.lowHintCost,
                      message: "Letter",
                      onPressed: () {
                        widget.lowHintCallback.call();
                      })),
              Padding(padding: EdgeInsets.fromLTRB(8, 0, 8, 0)),
              Expanded(
                  flex: 1,
                  child: HintButton(
                      icon: FindWordsCrossword.high_money,
                      quantity: Coins.highHintCost,
                      message: "Word",
                      onPressed: () {
                        widget.highHintCallback();
                      })),
            ],
          )
        ],
      ),
    );
  }
}
