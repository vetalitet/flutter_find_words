import 'dart:math';

import 'package:findwords/components/base_painter.dart';
import 'package:findwords/data/enums/equals_enum.dart';
import 'package:findwords/data/enums/rect_status.dart';
import 'package:findwords/data/item_size_rect_items.dart';
import 'package:findwords/data/json/progress.dart';
import 'package:findwords/data/json/settings.dart';
import 'package:findwords/data/level_data.dart';
import 'package:findwords/data/rect_item.dart';
import 'package:findwords/data/xy_char.dart';
import 'package:findwords/shared_preferences/shared_preferences_manager.dart';
import 'package:findwords/utils/calculate_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';

import '../data/enums/rect_status.dart';

class WordTable extends StatefulWidget {
  LevelData data;
  int foundWordsCount;
  bool rebuildRequired;
  bool wordHintPressed;
  final Function(List<ProgressPoints>) wordFoundCallback;
  final Function(String) showWordHintCallback;
  final VoidCallback duplicateFoundCallback;

  WordTable(
      {Key key,
      @required this.foundWordsCount,
      @required this.data,
      this.rebuildRequired,
      this.wordHintPressed,
      this.wordFoundCallback,
      this.showWordHintCallback,
      this.duplicateFoundCallback})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _WordTable();
}

class _WordTable extends State<WordTable> {
  ItemSizeRectItems painterData;
  RectItem currentSelectedItem;

  int maxIndex = 1;
  List<RectItem> selectedItems = List();

  LevelData levelData;

  @override
  Widget build(BuildContext context) {
    levelData = widget.data;
    if (painterData == null || widget.rebuildRequired) {
      painterData = _measureData(levelData);
      widget.rebuildRequired = false;
    }
    if (widget.wordHintPressed) {
      updateHintWordColor(levelData);
      widget.wordHintPressed = false;
    }
    return Container(
      child: GestureDetector(
        onPanDown: (details) {
          handleDownCase(details.localPosition);
        },
        onPanUpdate: (details) {
          handleUpdateCase(details.localPosition);
        },
        onPanEnd: (details) {
          handleUpCase();
        },
        child: Container(
          child: CustomPaint(
            size: _getMinSize(
                context, levelData.chartSizeData.x, painterData.itemSize),
            painter: BasePainter(
              painterData,
            ),
          ),
        ),
      ),
    );
  }

  Size _getMinSize(
      BuildContext context, int wordTableCellsCount, double itemSize) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    if (width < 500) {
      var value = min(width, height);
      return Size(value, value);
    } else {
      var value = min(width, height);
      var areaHeight = (wordTableCellsCount + 4) * itemSize;
      return Size(value, areaHeight);
    }
  }

  ItemSizeRectItems _measureData(LevelData data) {
    List<RectItem> rectItems = List();
    double wordTableSize = getWordTableSize(context, data.chartSizeData.x);
    double itemSize = (wordTableSize / data.chartSizeData.x) - 1;
    Size areaSize = _getMinSize(context, data.chartSizeData.x, itemSize);
    double areaSizeX = areaSize.width;
    double areaSizeY = areaSize.height;
    double initPosX = (areaSizeX - wordTableSize) / 2;
    double initPosY = (areaSizeY - wordTableSize) / 2;
    _runOverData(data, callback: (int x, int y) {
      double left = itemSize * (y - 1) + 1 + initPosX.toInt();
      double top = itemSize * (x - 1) + 1 + initPosY.toInt();

      String char = data.getCharBy(y, x);
      int status = data.getStatusBy(y, x);
      int color = data.getColorBy(y, x);

      int defaultColor = Color.fromRGBO(255, 196, 0, 1).value;

      Rect rect = Rect.fromLTRB(left.toDouble(), top.toDouble(),
          (left + itemSize).toDouble(), (top + itemSize).toDouble());
      RectItem rectItem = RectItem(rect, char, defaultColor, y, x);
      rectItem.status = getStatusByValue(status);
      if (color.abs() > 0) {
        rectItem.color = color;
      }
      rectItems.add(rectItem);
    });
    var points = generatePoints(
        initPosX + 1,
        initPosY + 1,
        data.chartSizeData.x,
        itemSize,
        areaSizeX,
        areaSizeY,
        wordTableSize.toInt());
    return ItemSizeRectItems(itemSize, rectItems, points.item1, points.item2);
  }

  Tuple2<List<Tuple2<Point, Point>>, List<Tuple2<Point, Point>>> generatePoints(
      double posX,
      double posY,
      int itemsCount,
      double itemSize,
      double areaSizeX,
      double areaSizeY,
      int wordTableSize) {
    List<Tuple2<Point, Point>> horizontalPoints = List();
    List<Tuple2<Point, Point>> verticalPoints = List();
    var spaceBetween = 4;
    for (var i = 1; i < itemsCount + 2; i++) {
      if (i == 1) {
        var hp1 = Point(0, posY + spaceBetween);
        var hp2 = Point(areaSizeX, posY + spaceBetween);
        horizontalPoints.add(Tuple2(hp1, hp2));

        var vp1 = Point(posX + spaceBetween, areaSizeY);
        var vp2 = Point(posX + spaceBetween, 0);
        verticalPoints.add(Tuple2(vp1, vp2));
      } else if (i == itemsCount + 1) {
        var hp1 = Point(0, (posY + itemSize * (i - 1)) - spaceBetween);
        var hp2 = Point(areaSizeX, (posY + itemSize * (i - 1)) - spaceBetween);
        horizontalPoints.add(Tuple2(hp1, hp2));

        var vp1 = Point((posX + itemSize * (i - 1)) - spaceBetween, areaSizeY);
        var vp2 = Point((posX + itemSize * (i - 1)) - spaceBetween, 0);
        verticalPoints.add(Tuple2(vp1, vp2));
      } else {
        var hp1 = Point(0, (posY + itemSize * (i - 1)) - spaceBetween);
        var hp2 = Point(areaSizeX, (posY + itemSize * (i - 1)) - spaceBetween);
        horizontalPoints.add(Tuple2(hp1, hp2));

        var hp3 = Point(0, (posY + itemSize * (i - 1)) + spaceBetween);
        var hp4 = Point(areaSizeX, (posY + itemSize * (i - 1)) + spaceBetween);
        horizontalPoints.add(Tuple2(hp3, hp4));

        var vp1 = Point((posX + itemSize * (i - 1)) - spaceBetween, areaSizeY);
        var vp2 = Point((posX + itemSize * (i - 1)) - spaceBetween, 0);
        verticalPoints.add(Tuple2(vp1, vp2));

        var vp3 = Point((posX + itemSize * (i - 1)) + spaceBetween, areaSizeY);
        var vp4 = Point((posX + itemSize * (i - 1)) + spaceBetween, 0);
        verticalPoints.add(Tuple2(vp3, vp4));
      }
    }

    var i = posY - itemSize - 1;
    while (i > 0) {
      var hp1 = Point(0, i);
      var hp2 = Point(areaSizeX, i);
      horizontalPoints.add(Tuple2(hp1, hp2));
      i = i - itemSize;
    }

    var i1 = posY + wordTableSize + itemSize;
    while (i1 < areaSizeY) {
      var hp1 = Point(0, i1);
      var hp2 = Point(areaSizeX, i1);
      horizontalPoints.add(Tuple2(hp1, hp2));
      i1 = i1 + itemSize;
    }

    var hp1 = Point(0, areaSizeY);
    var hp2 = Point(areaSizeX, areaSizeY);
    horizontalPoints.add(Tuple2(hp1, hp2));

    var i2 = posX - itemSize - 1;
    while (i2 > 0) {
      var vp1 = Point(i2, 0);
      var vp2 = Point(i2, areaSizeY);
      verticalPoints.add(Tuple2(vp1, vp2));
      i2 = i2 - itemSize;
    }

    var i3 = posX + wordTableSize + itemSize;
    while (i3 < areaSizeX) {
      var vp1 = Point(i3, 0);
      var vp2 = Point(i3, areaSizeY);
      verticalPoints.add(Tuple2(vp1, vp2));
      i3 = i3 + itemSize;
    }

    return Tuple2(horizontalPoints, verticalPoints);
  }

  _runOverData(LevelData data, {Function(int, int) callback}) {
    var x = 1;
    var y = 1;

    while (x <= data.chartSizeData.x) {
      while (y <= data.chartSizeData.y) {
        callback(x, y);
        y++;
      }
      x++;
      y = 1;
    }
  }

  RectStatus getStatusByValue(int status) {
    if (status == RectStatus.NONE.index) {
      return RectStatus.NONE;
    } else if (status == RectStatus.SELECTED.index) {
      return RectStatus.SELECTED;
    } else if (status == RectStatus.IS_SELECTING.index) {
      return RectStatus.IS_SELECTING;
    }
    return null;
  }

  void releaseAll() {
    painterData.rectItems
        .where((element) => element.status == RectStatus.IS_SELECTING)
        .forEach((element) {
      element.clearItem();
    });
    currentSelectedItem = null;
    selectedItems.clear();
    maxIndex = 1;
  }

  void handleUpCase() {
    setState(() {
      handleCorrectWord();
      releaseAll();
    });
  }

  void handleUpdateCase(Offset position) {
    setState(() {
      painterData.rectItems
          .where((element) =>
              element.status != RectStatus.NONE &&
              element.rect.contains(position))
          .forEach((element) {
        currentSelectedItem = element;
      });

      var intersectRects = painterData.rectItems.where((element) =>
          element.status == RectStatus.NONE &&
          element.rect.contains(position) &&
          currentSelectedItem.index == maxIndex - 1);
      intersectRects.forEach((element) {
        element.status = RectStatus.IS_SELECTING;
        selectedItems.add(element);
        selectedItems[selectedItems.length - 1].index = maxIndex;
        currentSelectedItem = element;
        maxIndex++;
      });
    });
  }

  void handleDownCase(Offset position) {
    setState(() {
      painterData.rectItems
          .where((element) => element.status == RectStatus.IS_SELECTING)
          .forEach((element) {
        element.status = RectStatus.NONE;
      });

      painterData.rectItems
          .where((element) =>
              element.status == RectStatus.NONE &&
              element.rect.contains(position))
          .forEach((element) {
        element.index = maxIndex;
        element.status = RectStatus.IS_SELECTING;
        selectedItems.add(element);
        currentSelectedItem = element;
        currentSelectedItem.index = maxIndex;
        maxIndex++;
      });
    });
  }

  void handleCorrectWord() {
    List<XYChar> selectedWord = getSelectedWord();
    Tuple2<int, EqualsEnum> matchedWordPair =
        levelData.checkContainsWord(selectedWord);
    int wordNumber = matchedWordPair.item1;
    EqualsEnum equalsEnum = matchedWordPair.item2;
    int newGeneratedColor = generateColor(levelData.menuItemNumber);
    if (equalsEnum == EqualsEnum.EQUALS_COMPLETELY) {
      painterData.rectItems
          .where((element) => element.status == RectStatus.IS_SELECTING)
          .forEach((element) {
        element.status = RectStatus.SELECTED;
        element.color = newGeneratedColor;
      });
      selectedItems.clear();
      callWordFoundCallback(selectedWord, newGeneratedColor);
    } else if (equalsEnum == EqualsEnum.EQUALS_BUT_WRONG_POSITIONS) {
      widget.duplicateFoundCallback();
    }
  }

  List<XYChar> getSelectedWord() {
    List<XYChar> word = List<XYChar>();
    selectedItems.forEach((element) {
      word.add(XYChar(element.x, element.y, element.char));
    });
    return word;
  }

  int generateColor(int menuItemNumber) {
    return colorsArray[widget.foundWordsCount].value;
  }

  var colorsArray = [
    Color.fromARGB(255, 255, 145, 132),
    Color.fromARGB(255, 201, 214, 123),
    Color.fromARGB(255, 76, 189, 190),
    Color.fromARGB(255, 22, 167, 82),
    Color.fromARGB(255, 229, 109, 188),
    Color.fromARGB(255, 35, 173, 255),
    Color.fromARGB(255, 198, 143, 102),
    Color.fromARGB(255, 151, 170, 241),
    Color.fromARGB(255, 192, 141, 191),
    Color.fromARGB(255, 96, 142, 183),
    Color.fromARGB(255, 221, 136, 133),
    Color.fromARGB(255, 122, 158, 156),
  ];

  void callWordFoundCallback(List<XYChar> selectedWord, int color) {
    List<ProgressPoints> points = List();
    selectedWord.forEach((element) {
      points.add(ProgressPoints(element.x, element.y, color));
    });
    widget.wordFoundCallback.call(points);
  }

  void updateHintWordColor(LevelData levelData) {
    int newGeneratedColor = generateColor(levelData.menuItemNumber);
    Settings settings = SharedPreferenceManager.getSettings();

    String foundWord = "";

    levelData.words.forEach((word) {
      word.items.forEach((item) {
        if (item.status != RectStatus.SELECTED.index &&
            item.status != RectStatus.IS_WORD_HINT.index) {
          painterData.rectItems
              .where((element) => element.x == item.x && element.y == item.y)
              .forEach((element) {
            element.status = RectStatus.NONE;
          });
        }
        if (item.status == RectStatus.IS_WORD_HINT.index) {
          painterData.rectItems
              .where((element) => element.x == item.x && element.y == item.y)
              .forEach((element) {
            if (settings.isHintShown) {
              element.status = RectStatus.SELECTED;
              element.color = newGeneratedColor;
              selectedItems.add(element);
            } else {
              element.status = RectStatus.NONE;
            }
            foundWord += element.char;
          });
        }
      });
    });
    if (settings.isHintShown) {
      widget.foundWordsCount++;
    }

    final List<XYChar> selectedWord = getSelectedWord();
    selectedItems.clear();
    widget.showWordHintCallback(foundWord);
    if (settings.isHintShown) {
      callWordFoundCallback(selectedWord, newGeneratedColor);
    }
  }
}

class CustomPanGestureRecognizer extends OneSequenceGestureRecognizer {
  final Function onPanDown;
  final Function onPanUpdate;
  final Function onPanEnd;

  CustomPanGestureRecognizer({
    @required this.onPanDown,
    @required this.onPanUpdate,
    @required this.onPanEnd,
  });

  @override
  void addPointer(PointerEvent event) {
    if (onPanDown(event.position)) {
      startTrackingPointer(event.pointer);
      resolve(GestureDisposition.accepted);
    } else {
      stopTrackingPointer(event.pointer);
    }
  }

  @override
  void handleEvent(PointerEvent event) {
    if (event is PointerMoveEvent) {
      onPanUpdate(event.position);
    }
    if (event is PointerUpEvent) {
      onPanEnd(event.position);
      stopTrackingPointer(event.pointer);
    }
  }

  @override
  String get debugDescription => 'customPan';

  @override
  void didStopTrackingLastPointer(int pointer) {}
}
