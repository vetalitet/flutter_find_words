import 'package:findwords/mycolors.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class HintButton extends StatelessWidget {
  HintButton({
    @required this.icon,
    @required this.quantity,
    @required this.message,
    @required this.onPressed,
  });
  final IconData icon;
  final int quantity;
  final String message;
  final GestureTapCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      fillColor: MyColors.buttonBlue,
      splashColor: Colors.blueAccent,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(
              icon,
              color: MyColors.splash_color_2,
            ),
            Padding(padding: EdgeInsets.only(left: 4)),
            Text(
              "$quantity",
              maxLines: 1,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
            SizedBox(width: 16.0),
            Text(
              message,
              maxLines: 1,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          ],
        ),
      ),
      onPressed: onPressed,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(4.0))),
    );
  }
}
