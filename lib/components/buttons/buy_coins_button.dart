import 'package:findwords/mycolors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class BuyCoinsButton extends StatelessWidget {
  final int coins;
  final VoidCallback onPress;

  BuyCoinsButton({Key key, @required this.coins, @required this.onPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      child: Row(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: MyColors.buyCoinsButtonPurple,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(6),
                bottomLeft: Radius.circular(6),
              ),
            ),
            height: 40.0,
            child: Row(
              children: <Widget>[
                Container(
                  child: SvgPicture.asset('images/vector_money.svg'),
                  margin: EdgeInsets.only(left: 8.0),
                  width: 20.0,
                  height: 20.0,
                ),
                SizedBox(
                  width: 4.0,
                ),
                Container(
                  width: 62.0,
                  child: Center(
                    child: Text(
                      "$coins",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(width: 2),
          Container(
            decoration: BoxDecoration(
              color: MyColors.buyCoinsButtonPurpleDark,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(6),
                bottomRight: Radius.circular(6),
              ),
            ),
            height: 40.0,
            child: Container(
              child: SvgPicture.asset(
                'images/vector_plus.svg',
                width: 20,
                height: 20,
              ),
              margin: EdgeInsets.only(left: 8.0, right: 8.0),
            ),
          ),
        ],
      ),
      onPressed: () {
        if (onPress != null) {
          onPress.call();
        }
      },
    );
  }
}
