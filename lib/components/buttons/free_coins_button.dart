import 'package:findwords/mycolors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FreeCoinsButton extends StatelessWidget {
  final int coinsQuantity;
  final int buttonPrice;
  final bool isEnabled;
  final VoidCallback onClick;

  FreeCoinsButton(
      {Key key,
      @required this.coinsQuantity,
      @required this.buttonPrice,
      @required this.isEnabled,
      this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      elevation: 0,
      onPressed: () {
        if (onClick != null) {
          onClick();
        }
      },
      child: Container(
        width: 140,
        height: 40,
        child: Row(
          children: [
            Container(
              decoration: BoxDecoration(
                color: isEnabled ? MyColors.buyCoinsButtonPurple : MyColors.buyCoinsButtonDisabledGray,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(6),
                  bottomLeft: Radius.circular(6),
                ),
              ),
              height: 42.0,
              child: Row(
                children: [
                  Container(
                    child: SvgPicture.asset('images/vector_money.svg'),
                    margin: EdgeInsets.only(left: 8.0),
                    width: 20.0,
                    height: 20.0,
                  ),
                  SizedBox(
                    width: 2.0,
                  ),
                  Container(
                    width: 58.0,
                    child: Center(
                      child: Text(
                        "$coinsQuantity",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                color: isEnabled ? MyColors.buyCoinsButtonPurpleDark : MyColors.buyCoinsButtonDisabledGray,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(6),
                  bottomRight: Radius.circular(6),
                ),
              ),
              height: 42.0,
              child: Container(
                width: 40.0,
                child: Center(
                  child: Text(
                    "$buttonPrice\$",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
