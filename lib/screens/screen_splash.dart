import 'package:findwords/mycolors.dart';
import 'package:findwords/screens/screen_menu.dart';
import 'package:findwords/utils/preload_utils.dart';
import 'package:flutter/material.dart';
import 'package:loading_animations/loading_animations.dart';

class SplashScreen extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<SplashScreen> {
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [MyColors.bgColorTop, MyColors.bgColorBottom]))),
          Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(),
                ),
                Image(image: AssetImage("images/splash.png")),
                Expanded(
                  flex: 1,
                  child: Container(
                    child: LoadingBouncingGrid.square(
                      size: 30,
                      backgroundColor: MyColors.splash_color_2,
                    ),
                  ),
                ),
              ],
            ),
          ),
          FutureBuilder<bool>(
            future: loadingSvgs(),
            builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData && snapshot.data) {
                _navigateToMenuScreen();
              }
              return Text("");
            },
          )
        ],
      ),
    );
  }

  Future<bool> loadingSvgs() async {
    await Future.wait([
      loadSvg(context, 'images/ic_upvote.svg'),
      loadSvg(context, 'images/ic_klapovuhyi.svg'),
      loadSvg(context, 'images/ic_medal.svg'),
      loadSvg(context, 'images/vector_info.svg'),
      loadSvg(context, 'images/i1.svg'),
      loadSvg(context, 'images/i2.svg'),
      loadSvg(context, 'images/i3.svg'),
      loadSvg(context, 'images/i4.svg'),
      loadSvg(context, 'images/i5.svg'),
      loadSvg(context, 'images/i1c.svg'),
      loadSvg(context, 'images/i2c.svg'),
      loadSvg(context, 'images/i3c.svg'),
      loadSvg(context, 'images/i4c.svg'),
      loadSvg(context, 'images/i5c.svg'),
    ]);
    return true;
  }

  void _navigateToMenuScreen() {
    Future.microtask(() => Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (BuildContext context) => MenuScreen())));
  }
}
