import 'package:findwords/components/toolbar_select_category.dart';
import 'package:findwords/data/json/settings.dart';
import 'package:findwords/mycolors.dart';
import 'package:findwords/shared_preferences/shared_preferences_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreen createState() => _SettingsScreen();
}

class _SettingsScreen extends State<SettingsScreen> {
  Settings _settings;

  @override
  void initState() {
    setState(() {
      _settings = SharedPreferenceManager.getSettings();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Material(
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [MyColors.menuHeader, MyColors.bgColorBottom])),
          child: Column(
            children: [
              ToolbarSelectCategory(
                title: "Settings",
                onPress: () {},
              ),
              Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(16, 20, 8, 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text(
                              "Show hint word visually on the grid, after pressing \'Word hint\' button",
                              maxLines: 3,
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                          Checkbox(
                            activeColor: MyColors.menuHeader,
                            value: _settings.isHintShown,
                            onChanged: (bool newValue) {
                              setState(() {
                                _settings.isHintShown = newValue;
                                SharedPreferenceManager.setSettings(_settings);
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(16, 20, 8, 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text(
                              "Show additional playing info in header (like, found words quantity and current level)",
                              maxLines: 3,
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                          Checkbox(
                            activeColor: MyColors.menuHeader,
                            value: _settings.isAdditionalInfoShown,
                            onChanged: (bool newValue) {
                              setState(() {
                                _settings.isAdditionalInfoShown = newValue;
                                SharedPreferenceManager.setSettings(_settings);
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
