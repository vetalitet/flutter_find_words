import 'dart:collection';
import 'dart:math';

import 'package:custom_radio_grouped_button/CustomButtons/ButtonTextStyle.dart';
import 'package:custom_radio_grouped_button/CustomButtons/CustomRadioButton.dart';
import 'package:findwords/components/toolbar_select_category.dart';
import 'package:findwords/data/chart_size_data.dart';
import 'package:findwords/data/file_parser.dart';
import 'package:findwords/data/item_data.dart';
import 'package:findwords/data/level_data.dart';
import 'package:findwords/data/menu_level.dart';
import 'package:findwords/data/word_data.dart';
import 'package:findwords/data/xy_char.dart';
import 'package:findwords/mycolors.dart';
import 'package:findwords/screens/screen_main.dart';
import 'package:findwords/screens/screen_select_category.dart';
import 'package:findwords/shared_preferences/shared_preferences_manager.dart';
import 'package:findwords/strings.dart';
import 'package:findwords/utils/preload_utils.dart';
import 'package:findwords/utils/utils.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RandomScreenMenu extends StatefulWidget {
  final MobileAdTargetingInfo targetingInfo;

  RandomScreenMenu({Key key, @required this.targetingInfo}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<RandomScreenMenu> {
  final labelValues = ["5", "6", "7"];
  int _selectedLevel;

  List<MenuItem> _menuItems;
  MenuItem _selectedItem;
  bool _isLoadVisible = false;

  @override
  void initState() {
    _selectedLevel = int.parse(labelValues[0]);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Future.value(loadLevels(context, Utils.baseLevelWordsPath))
          .then((levels) {
        setState(() {
          _menuItems = levels;
          _selectedItem = _menuItems[0];
        });
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Material(
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [MyColors.menuHeader, MyColors.bgColorBottom])),
          child: Stack(
            children: [
              Column(
                children: [
                  ToolbarSelectCategory(
                    title: Strings.menu_campaign_level,
                    onPress: () {},
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 36)),
                  SvgPicture.asset(
                    "images/puzzle.svg",
                    width: 64,
                    height: 64,
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 16)),
                  Text(
                    "Choose details",
                    style: TextStyle(
                        fontSize: 24,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                  Expanded(
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                              width: 315,
                              child: Text("Select grid size",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                ),
                              ),
                          ),
                          Padding(
                              padding:
                              EdgeInsets.only(bottom: 4.0)),
                          Container(
                            width: 325,
                            height: 55,
                            child: CustomRadioButton(
                              elevation: 0,
                              absoluteZeroSpacing: false,
                              selectedColor: MyColors.splash_color_1,
                              unSelectedColor: MyColors.splash_color_2,
                              buttonLables: [
                                '5x5',
                                '6x6',
                                '7x7',
                              ],
                              buttonValues: labelValues,
                              defaultSelected: labelValues[0],
                              buttonTextStyle: ButtonTextStyle(
                                  selectedColor: Colors.white,
                                  unSelectedColor: Colors.grey[800],
                                  textStyle: TextStyle(fontSize: 20)),
                              radioButtonValue: (value) {
                                setState(() {
                                  _selectedLevel = int.parse(value);
                                });
                              },
                              height: 60,
                            ),
                          ),
                          Padding(
                              padding:
                                  EdgeInsets.only(top: 16.0, bottom: 8.0)),
                          Container(
                            width: 315,
                            child: Text("Select category",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                              ),
                            ),
                          ),
                          Padding(
                              padding:
                              EdgeInsets.only(bottom: 8.0)),
                          InkWell(
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4.0),
                                color: Colors.amber[400],
                              ),
                              child: Container(
                                width: 315,
                                child: Row(
                                  children: [
                                    Text(
                                      _selectedItem == null
                                          ? ""
                                          : _selectedItem.name,
                                      style: TextStyle(
                                        color: Colors.grey[850],
                                        fontSize: 18,
                                      ),
                                    ),
                                    Spacer(),
                                    SvgPicture.asset(
                                      'images/triangle.svg',
                                      width: 10,
                                      height: 10,
                                    )
                                  ],
                                ),
                                padding:
                                    const EdgeInsets.fromLTRB(22, 10, 15, 10),
                              ),
                            ),
                            onTap: () {
                              _menuItems[_selectedItem.index - 1].status =
                                  MenuItemStatus.ENABLED;
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SelectCategoryScreen(
                                          menuItems: _menuItems,
                                          onItemSelected: (MenuItem menuItem) {
                                            setState(() {
                                              _menuItems.forEach((element) {
                                                element.status =
                                                    MenuItemStatus.DISABLED;
                                              });
                                              _selectedItem = menuItem;
                                            });
                                          },
                                        )),
                              );
                            },
                          ),
                          Padding(
                              padding:
                                  EdgeInsets.only(top: 16.0, bottom: 24.0)),
                          SizedBox(
                            width: 150,
                            height: 55,
                            child: RaisedButton(
                              onPressed: () {
                                if (!_isLoadVisible) {
                                  setState(() {
                                    _isLoadVisible = true;
                                  });
                                  Future.delayed(
                                      const Duration(milliseconds: 200), () {
                                    Future.value(_createLevelData())
                                        .then((value) {
                                      Navigator.pop(context);
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => MainScreen(
                                                  index: 1,
                                                  levelName: _selectedItem.name,
                                                  levelDataList: value,
                                                  targetingInfo:
                                                      widget.targetingInfo,
                                                  progress: SharedPreferenceManager
                                                      .getProgress(
                                                          SharedPreferenceManager
                                                              .RANDOM_PROGRESS_KEY),
                                                  updateCoins: () {},
                                                  saveProgressLevelState: () {},
                                                  categotyCompleted: () {
                                                    SharedPreferenceManager
                                                        .deleteProgress(
                                                            SharedPreferenceManager
                                                                .RANDOM_PROGRESS_KEY);
                                                  },
                                                )),
                                      );
                                    });
                                  });
                                }
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(6.0)),
                              ),
                              child: Text(
                                "Start",
                                style: TextStyle(
                                    fontSize: 24.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey[800]),
                              ),
                              textColor: Colors.white,
                              splashColor: Colors.red,
                              color: Colors.amber[400],
                            ),
                          ),
                          Visibility(
                              visible: _isLoadVisible,
                              maintainSize: true,
                              maintainAnimation: true,
                              maintainState: true,
                              child: Container(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 20.0),
                                  child: Text(
                                    "Loading...",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              )),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<List<LevelData>> _createLevelData() async {
    Map<int, List<String>> levelWords = await FileReader().parseLevelWords(
        "${Utils.baseLevelWordsPath}/${_selectedItem.index}.${_selectedItem.name}");
    List<RandomLevel> levels = await FileReader()
        .parseLevelRoutes("${Utils.baseLevelRoutesPath}/$_selectedLevel");

    Random random = new Random();

    int randomLevelIndex = random.nextInt(levels.length);
    RandomLevel randomLevel = levels[randomLevelIndex];

    int cycleNumber = random.nextInt(4);
    for (int i = 0; i < cycleNumber; i++) {
      randomLevel = rorateLevel(randomLevel);
    }

    Set<String> foundWords = HashSet();
    randomLevel.words.forEach((word) {
      int currentWordLength = word.points.length;
      List<String> wordsByLength = levelWords[currentWordLength];

      String wordbyLength;
      do {
        int randomWordIndex = random.nextInt(wordsByLength.length);
        wordbyLength = wordsByLength[randomWordIndex];
      } while (foundWords.contains(wordbyLength));

      foundWords.add(wordbyLength);
      for (int i = 0; i < word.points.length; i++) {
        word.points[i].char = wordbyLength[i];
      }
    });

    var words = List<WordData>();
    for (int i = 0; i < randomLevel.words.length; i++) {
      RandomWord word = randomLevel.words[i];
      var items = List<ItemData>();
      for (int j = 0; j < word.points.length; j++) {
        XYChar point = word.points[j];
        var item = ItemData(point.x, point.y, j, i + 1, 0, 0, point.char);
        items.add(item);
      }
      words.add(WordData(items));
    }

    var chartSizeData = ChartSizeData(_selectedLevel, _selectedLevel);
    LevelData levelData = LevelData(chartSizeData, 1, 1, words);
    List<LevelData> result = List();
    result.add(levelData);
    return Future.value(result);
  }

  RandomLevel rorateLevel(RandomLevel randomLevel) {
    var m = _generateLevel(randomLevel);
    _rotateMatrix(m);
    Map<int, List<MI>> map = convertMatrixToWordsMap(m);
    Map<int, List<MI>> sortedMap = sortWordsMapAscending(map);
    return convertToLevel(sortedMap);
  }

  List<List<MI>> _generateLevel(RandomLevel randomLevel) {
    var m = List.generate(_selectedLevel,
        (i) => List.generate(_selectedLevel, (j) => MI(0, 0, 0, 0)));
    int wordIndex = 0;
    randomLevel.words.forEach((word) {
      var index = 0;
      word.points.forEach((point) {
        m[point.y - 1][point.x - 1] = MI(point.x, point.y, index, wordIndex);
        index++;
      });
      wordIndex++;
    });
    return m;
  }

  void _rotateMatrix(List<List<MI>> mat) {
    int N = _selectedLevel;
    for (int x = 0; x < N / 2; x++) {
      for (int y = x; y < N - x - 1; y++) {
        MI temp = mat[x][y];
        mat[x][y] = mat[y][N - 1 - x];
        mat[y][N - 1 - x] = mat[N - 1 - x][N - 1 - y];
        mat[N - 1 - x][N - 1 - y] = mat[N - 1 - y][x];
        mat[N - 1 - y][x] = temp;
      }
    }
  }

  Map<int, List<MI>> convertMatrixToWordsMap(List<List<MI>> m) {
    final Map<int, List<MI>> resultMap = Map();
    for (var i = 0; i < m.length; i++) {
      var row = m[i];
      for (var j = 0; j < row.length; j++) {
        var item = row[j];
        var value = resultMap[item.word];
        if (value == null) {
          value = List();
        }
        value.add(MI(j + 1, i + 1, item.index, item.word));
        resultMap[item.word] = value;
      }
    }
    return resultMap;
  }

  Map<int, List<MI>> sortWordsMapAscending(Map<int, List<MI>> map) {
    map.forEach((key, value) {
      value.sort((a, b) => a.index.compareTo(b.index));
    });
    return map;
  }

  RandomLevel convertToLevel(Map<int, List<MI>> sortedMap) {
    List<RandomWord> words = List();
    sortedMap.forEach((key, value) {
      List<XYChar> points = List();
      value.forEach((item) {
        points.add(XYChar(item.x, item.y, ""));
      });
      words.add(RandomWord(points: points));
    });
    return RandomLevel(words: words);
  }
}

class MI {
  final int x, y;
  final int index, word;
  MI(this.x, this.y, this.index, this.word);

  @override
  String toString() {
    return "($x, $y) [$index, $word]";
  }
}
