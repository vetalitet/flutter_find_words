import 'package:findwords/components/toolbar_select_category.dart';
import 'package:findwords/data/menu_level.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SelectCategoryScreen extends StatefulWidget {
  final List<MenuItem> menuItems;
  final Function(MenuItem) onItemSelected;

  SelectCategoryScreen({
    Key key,
    @required this.menuItems,
    @required this.onItemSelected,
  }) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<SelectCategoryScreen> {
  List<MenuItem> menuItems;

  @override
  void initState() {
    this.menuItems = widget.menuItems;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Material(
        child: Container(
            child: Column(
          children: <Widget>[
            ToolbarSelectCategory(title: "Select category", onPress: () {}),
            Expanded(
              child: Container(
                  child: (menuItems != null && menuItems.isNotEmpty)
                      ? createListView(context, menuItems)
                      : createLoadingPlaceholder()),
            ),
          ],
        )),
      ),
    );
  }

  Widget createListView(BuildContext context, List<MenuItem> menuItems) {
    return ListView.separated(
        separatorBuilder: (context, index) => Divider(
              height: 1,
            ),
        itemCount: menuItems.length,
        itemBuilder: (BuildContext context, int index) {
          var menuItem = menuItems[index];
          return InkWell(
            onTap: () {
              if (widget.onItemSelected != null) {
                widget.onItemSelected(menuItem);
              }
              Navigator.of(context).pop();
            },
            child: Container(
              height: 70.0,
              child: Stack(
                children: [
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: SvgPicture.asset('images/i${index + 1}.svg'),
                      ),
                      Text(
                        menuItem.name,
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Spacer(),
                      Stack(children: [
                        Visibility(
                            visible: menuItem.status == MenuItemStatus.ENABLED,
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 12),
                                child:
                                    SvgPicture.asset('images/ic_available.svg'),
                              ),
                            )),
                      ])
                    ],
                  )
                ],
              ),
            ),
          );
        });
  }

  Widget createLoadingPlaceholder() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            child: CircularProgressIndicator(),
            width: 60,
            height: 60,
          ),
          const Padding(
            padding: EdgeInsets.only(top: 16),
            child: Text('Awaiting result...'),
          )
        ],
      ),
    );
  }
}
