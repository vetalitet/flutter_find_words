import 'dart:async';
import 'dart:math';

import 'package:findwords/coins.dart';
import 'package:findwords/components/hints_area.dart';
import 'package:findwords/components/toolbar_main_screen.dart';
import 'package:findwords/components/word_table.dart';
import 'package:findwords/data/enums/rect_status.dart';
import 'package:findwords/data/item_data.dart';
import 'package:findwords/data/json/progress.dart';
import 'package:findwords/data/json/settings.dart';
import 'package:findwords/data/level_data.dart';
import 'package:findwords/data/word_data.dart';
import 'package:findwords/mycolors.dart';
import 'package:findwords/shared_preferences/shared_preferences_manager.dart';
import 'package:findwords/strings.dart';
import 'package:findwords/utils/advert_utils.dart';
import 'package:findwords/utils/calculate_utils.dart';
import 'package:findwords/utils/dialogs_utils.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import '../data/enums/rect_status.dart';
//import 'package:in_app_purchase/in_app_purchase.dart';

class MainScreen extends StatefulWidget {
  final int index;
  final String levelName;
  final List<LevelData> levelDataList;
  final MobileAdTargetingInfo targetingInfo;
  final Progress progress;

  final Function() saveProgressLevelState;
  final VoidCallback categotyCompleted;
  final VoidCallback updateCoins;

  MainScreen(
      {Key key,
      @required this.index,
      @required this.levelName,
      @required this.levelDataList,
      @required this.targetingInfo,
      @required this.progress,
      this.saveProgressLevelState,
      this.categotyCompleted,
      this.updateCoins})
      : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  List<LevelData> levelDataList;
  int menuNumber;
  Progress progress;
  ProgressLevel progressLevel;
  Settings settings;

  bool isRandomLevel;
  bool isLetterHintPressed = false;
  bool isWordHintPressed = false;
  String _hintWord;
  Random random = new Random();

  bool isRewardedLoaded = false;

  @override
  void initState() {
    menuNumber = widget.index;
    progress = widget.progress;
    progressLevel = progress.findProgressLevelById(widget.index - 1);
    levelDataList = widget.levelDataList;
    settings = SharedPreferenceManager.getSettings();
    isRandomLevel =
        progress.progressId == SharedPreferenceManager.RANDOM_PROGRESS_KEY;
    super.initState();
    AdvertUtils.handleRewardFlow(widget.targetingInfo, onLoaded: (bool isLoaded) {
      setState(() {
        this.isRewardedLoaded = isLoaded;
      });
    }, onRewardedSuccess: (int rewardedCoins) {
      FirebaseAnalytics().logEvent(name: 'Game. Rewarded', parameters: null);
      updateStateWithCoins(rewardedCoins);
    });
    /*purchaseBloc.initCallbackHandler((PurchaseDetails purchaseDetails) {
      if (purchaseDetails.productID == "letter_hint") {
        updateStateWithCoins(Coins.lowReward);
      } else if (purchaseDetails.productID == "word_hint") {
        updateStateWithCoins(Coins.highReward);
      }
    });*/
  }

  Future<bool> _onBackPressed() {
    if (isRandomLevel && progressLevel.foundWordsCount > 0) {
      return _showConfirmExitDialog() ?? false;
    } else {
      return Future.value(true);
    }
  }

  Future<bool> _showConfirmExitDialog() {
    return DialogUtils.showBuyLevelDialog(
        context,
        'images/vector_info.svg',
        "Oh, no!",
        "In case you quit,\nyour progress will not be saved.",
        "Just quit", callback: () {
      SharedPreferenceManager.deleteProgress(
          SharedPreferenceManager.RANDOM_PROGRESS_KEY);
      Navigator.of(context).pop(true);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: SafeArea(
        child: Material(
          child: Container(
            decoration: BoxDecoration(
              color: MyColors.levelBg,
            ),
            child: _buildMainScreen(),
          ),
        ),
      ),
    );
  }

  LevelData updateDataIfHintsSelected(LevelData levelData) {
    if (isLetterHintPressed) {
      final List<WordData> words = findNotSelectedWords(levelData.words);
      final randomIndex = random.nextInt(words.length);
      words[randomIndex].items[0].status = RectStatus.IS_SELECTING.index;
    } else if (isWordHintPressed) {
      levelData.words.forEach((word) {
        var item = word.items[0];
        if (item.status != RectStatus.SELECTED.index) {
          item.status = RectStatus.NONE.index;
        }
      });
      final List<WordData> words = findNotSelectedWords(levelData.words);
      final randomIndex = random.nextInt(words.length);
      var items = words[randomIndex].items;
      for (int i = 0; i < items.length; i++) {
        words[randomIndex].items[i].status = RectStatus.IS_WORD_HINT.index;
      }
    }
    return levelData;
  }

  Widget _buildMainScreen() {
    if (progressLevel.currentSublevel >= levelDataList.length) {
      return Text("");
    }
    var selectedLevelData = levelDataList[progressLevel.currentSublevel];
    var levelData =
        getDataWithMergedSavedSnapshot(selectedLevelData, progressLevel);
    levelData = updateDataIfHintsSelected(levelData);
    return Column(
      children: <Widget>[
        ToolbarMainScreen(
          levelName: widget.levelName,
          levelIndex: progressLevel.currentSublevel + 1,
          totalLevelCount: levelDataList.length,
          wordsCount: widget.progress.wordsCount,
          backPress: () {
            if (isRandomLevel && progressLevel.foundWordsCount > 0) {
              _showConfirmExitDialog();
            } else {
              Navigator.of(context).pop(true);
            }
          },
          onPress: () {
            if (isRewardedLoaded) {
              showGetCoinsDialog("Get coins...");
            }
          },
        ),
        Container(
          alignment: Alignment.topLeft,
          decoration: BoxDecoration(
            color: MyColors.levelBg,
          ),
          child: WordTable(
            data: levelData,
            foundWordsCount: progressLevel.foundWordsCount,
            rebuildRequired: isRebuildRequires(),
            wordHintPressed: isWordHintPressed,
            wordFoundCallback: (List<ProgressPoints> progressPoints) {
              _hintWord = null;
              List<ProgressPoints> pointsToSave =
                  prepareListOfPoints(progressPoints);

              progressLevel.foundWordsCount++;
              progressLevel.progressPoints = pointsToSave;

              saveProgressToSharedPreferences(progressLevel.currentSublevel,
                  progressLevel.foundWordsCount, pointsToSave);

              checkAllCharsSelected(levelDataList.length, levelData);
              isWordHintPressed = false;
            },
            showWordHintCallback: (String word) {
              Future.delayed(
                Duration.zero,
                () {
                  if (settings.isHintShown) {
                    setState(() {});
                    Toast.show(word, context,
                        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                  } else {
                    _hintWord = word;
                    setState(() {});
                  }
                },
              );
              isWordHintPressed = false;
            },
            duplicateFoundCallback: () {
              Future.delayed(
                  Duration.zero,
                  () => DialogUtils.showCongratulationsDialog(
                      context,
                      'images/ic_klapovuhyi.svg',
                      Strings.hmm,
                      Strings.words_exists,
                      Strings.ok_lets_try,
                      160));
            },
          ),
        ),
        Padding(padding: EdgeInsets.only(bottom: 8)),
        Visibility(
            visible: _hintWord != null,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Your hint word: ",
                  style: TextStyle(fontSize: 18),
                ),
                Text(
                  "${_hintWord == null ? "" : _hintWord}".toUpperCase(),
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                )
              ],
            )),
        Spacer(),
        HintArea(
          width: getWordTableSize(context, levelData.chartSizeData.x),
          lowHintCallback: () {
            FirebaseAnalytics()
                .logEvent(name: 'Letter hint pressed', parameters: null);
            handleLetterHint();
          },
          highHintCallback: () {
            FirebaseAnalytics()
                .logEvent(name: 'Word hint pressed', parameters: null);
            handleWordHint();
          },
        ),
        Padding(padding: EdgeInsets.only(bottom: 24)),
      ],
    );
  }

  void saveProgressLevelState(int menuNumber,
      List<ProgressLevel> progressLevels, int coinsReward, int wordsToAdd) {
    progress.wordsCount += wordsToAdd;
    progress.updateProgressLevel(menuNumber, progressLevels);
    SharedPreferenceManager.setProgress(progress);
    SharedPreferenceManager.setCoins(
        SharedPreferenceManager.getCoins() + coinsReward);

    widget.saveProgressLevelState();
  }

  void saveProgressToSharedPreferences(int currentSublevel, int foundWordsCount,
      List<ProgressPoints> progressPoints) {
    progressLevel.foundWordsCount = foundWordsCount;
    progressLevel.progressPoints = progressPoints;
    saveProgressLevelState(menuNumber, List.of([progressLevel]), 0, 1);
  }

  LevelData getDataWithMergedSavedSnapshot(
      LevelData levelData, ProgressLevel progress) {
    var words = levelData.words;
    List<WordData> resultWordDataList = List();
    for (int i = 0; i < words.length; i++) {
      var items = levelData.words[i].items;
      List<ItemData> resultItemDataList = List();
      for (int j = 0; j < items.length; j++) {
        var elem = items[j];
        var foundColor = progress.containsElement(elem.x, elem.y);

        var oldItem = words[i].items[j];
        var resultItemData = ItemData(
            oldItem.x,
            oldItem.y,
            oldItem.charNumber,
            oldItem.wordNumber,
            foundColor > 0 ? RectStatus.SELECTED.index : RectStatus.NONE.index,
            foundColor > 0 ? foundColor : oldItem.color,
            oldItem.char);
        resultItemDataList.add(resultItemData);
      }
      WordData resultWordData = WordData(resultItemDataList);
      resultWordDataList.add(resultWordData);
    }
    LevelData resultLevelData = LevelData(levelData.chartSizeData,
        levelData.levelNumber, levelData.menuItemNumber, resultWordDataList);
    return resultLevelData;
  }

  List<ProgressPoints> prepareListOfPoints(List<ProgressPoints> inputPoints) {
    List<ProgressPoints> newPoints = List();
    List<ProgressPoints> oldPoints = progressLevel.progressPoints;
    if (oldPoints != null) {
      newPoints.addAll(oldPoints);
    }
    newPoints.addAll(inputPoints);
    return newPoints;
  }

  void checkAllCharsSelected(int levelDataCount, LevelData levelData) {
    bool levelCompleted = checkLevelCompleted(levelData);
    if (levelCompleted) {
      if (progressLevel.currentSublevel >= levelDataCount - 1) {
        progressLevel.currentSublevel++;
        progressLevel.foundWordsCount = 0;
        progressLevel.isCompleted = true;
        progressLevel.progressPoints = List();

        // enable next level
        ProgressLevel nextProgressLevel =
            ProgressLevel(progressLevel.menuItem + 1, 0, 0, false, List());

        saveProgressLevelState(
            menuNumber,
            List.of([progressLevel, nextProgressLevel]),
            Coins.categoryCompleted,
            0);

        Future.delayed(
            Duration.zero,
            () => DialogUtils.showCongratulationsDialog(
                    context,
                    'images/ic_medal.svg',
                    "Congratulations!",
                    "You have finished this category successfully",
                    "Choose other category",
                    240, callback: () {
                  setState(() {});
                  widget.categotyCompleted.call();
                  Navigator.of(context).pop();
                }));
        FirebaseAnalytics().logEvent(
            name: 'Category completed: ' + widget.levelName, parameters: null);
      } else {
        progressLevel.currentSublevel++;
        progressLevel.foundWordsCount = 0;
        progressLevel.progressPoints = List();
        saveProgressLevelState(
            menuNumber, List.of([progressLevel]), Coins.levelCompleted, 0);
        Future.delayed(
            Duration.zero,
            () => DialogUtils.showCongratulationsDialog(
                    context,
                    'images/ic_upvote.svg',
                    "Awesome!",
                    "You have done this level",
                    "Next Level",
                    160, callback: () {
                  setState(() {});
                }));
      }
    } else {
      if (!isWordHintPressed) {
        Future.delayed(Duration.zero, () => setState(() {}));
      }
    }
  }

  bool checkLevelCompleted(LevelData levelData) {
    int levelCharsNumber = 0;
    int progressCharsNumber = 0;
    levelData.words.forEach((word) {
      levelCharsNumber += word.items.length;
    });
    progressCharsNumber = progressLevel.progressPoints == null
        ? 0
        : progressLevel.progressPoints.length;
    return levelCharsNumber == progressCharsNumber;
  }

  void showGetCoinsDialog(String dialogText) {
    DialogUtils.showGetCoinsDialog(context, dialogText, freeButtonClick: () {
      RewardedVideoAd.instance.show();
    });
  }

  void updateStateWithCoins(int coins) {
    setState(() {
      SharedPreferenceManager.setCoins(
          SharedPreferenceManager.getCoins() + coins);
      widget.updateCoins();
    });
  }

  void handleLetterHint() {
    int neededCoins = Coins.lowHintCost;
    if (SharedPreferenceManager.getCoins() >= neededCoins) {
      setState(() {
        isLetterHintPressed = true;
      });
      updateStateWithCoins(-neededCoins);
    } else {
      if (isRewardedLoaded) {
        showGetCoinsDialog("Not enough coins. Get more...");
      } else {
        Toast.show("Not enough coins", context);
      }
    }
  }

  void handleWordHint() {
    int neededCoins = Coins.highHintCost;
    if (SharedPreferenceManager.getCoins() >= neededCoins) {
      setState(() {
        isWordHintPressed = true;
      });
      updateStateWithCoins(-neededCoins);
    } else {
      if (isRewardedLoaded) {
        showGetCoinsDialog("Not enough coins. Get more...");
      } else {
        Toast.show("Not enough coins", context);
      }
    }
  }

  bool isRebuildRequires() {
    bool isRebuildRequired =
        progressLevel.foundWordsCount == 0 || isLetterHintPressed;
    isLetterHintPressed = false;
    return isRebuildRequired;
  }

  List<WordData> findNotSelectedWords(List<WordData> words) {
    final List<WordData> result = List();
    words.forEach((word) {
      if (word.items[0].status != RectStatus.SELECTED.index) {
        result.add(word);
      }
    });
    return result;
  }
}
