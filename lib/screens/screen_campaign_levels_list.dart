import 'package:findwords/coins.dart';
import 'package:findwords/components/toolbar_wallet.dart';
import 'package:findwords/data/file_parser.dart';
import 'package:findwords/data/json/progress.dart';
import 'package:findwords/data/menu_level.dart';
import 'package:findwords/mycolors.dart';
import 'package:findwords/screens/screen_main.dart';
import 'package:findwords/shared_preferences/shared_preferences_manager.dart';
import 'package:findwords/utils/advert_utils.dart';
import 'package:findwords/utils/dialogs_utils.dart';
import 'package:findwords/utils/preload_utils.dart';
import 'package:findwords/utils/utils.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:toast/toast.dart';

import 'screen_main.dart';
//import 'package:in_app_purchase/in_app_purchase.dart';

class CampaignLevelsListScreen extends StatefulWidget {
  final MobileAdTargetingInfo targetingInfo;

  CampaignLevelsListScreen({Key key, @required this.targetingInfo})
      : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<CampaignLevelsListScreen> {
  Future<List<MenuItem>> menuItems;
  Progress progress;
  bool isRewardedAdsLoaded = false;

  @override
  void initState() {
    super.initState();
    menuItems = loadLevels(context, Utils.baseFilePath);
    Future.value(loadProgress()).then((value) {
      setState(() {
        this.progress = value;
      });
    });
    AdvertUtils.handleRewardFlow(widget.targetingInfo,
        onLoaded: (bool isLoaded) {
      setState(() {
        this.isRewardedAdsLoaded = isLoaded;
      });
    }, onRewardedSuccess: (int rewardedCoins) {
      FirebaseAnalytics().logEvent(name: 'List. Rewarded', parameters: null);
      updateStateWithCoins(rewardedCoins);
    });
    /*purchaseBloc.initCallbackHandler((PurchaseDetails purchaseDetails) {
      if (purchaseDetails.productID == "letter_hint") {
        updateStateWithCoins(Coins.lowReward);
      } else if (purchaseDetails.productID == "word_hint") {
        updateStateWithCoins(Coins.highReward);
      }
    });*/
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Material(
        child: Container(
            child: Column(
          children: <Widget>[
            ToolbarWallet(onPress: () {
              if (isRewardedAdsLoaded) {
                showGetCoinsDialog("Get more coins...");
              }
            }),
            Expanded(
              child: Container(
                  child: FutureBuilder<List<MenuItem>>(
                      future: menuItems,
                      builder: (context, snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return createLoadingPlaceholder();
                        } else {
                          return createListView(context,
                              mergeItemsWithProgress(snapshot.data, progress));
                        }
                      })),
            ),
          ],
        )),
      ),
    );
  }

  Widget createListView(BuildContext context, List<MenuItem> menuItems) {
    return ListView.separated(
        separatorBuilder: (context, index) => Divider(
              height: 1,
            ),
        itemCount: menuItems.length,
        itemBuilder: (BuildContext context, int index) {
          var menuItem = menuItems[index];
          var leftMenuColor = getColorByStatus(menuItem.status);
          return GestureDetector(
            onTap: () {
              if (menuItem.status == MenuItemStatus.ENABLED) {
                navigateToMainScreen(context, menuItem, menuItems.length);
              } else if (menuItem.status == MenuItemStatus.COMPLETED) {
                showLevelCompletedDialog();
              } else if (menuItem.status == MenuItemStatus.DISABLED) {
                showLevelIsDisabledDIalog(menuItem.index);
              }
            },
            child: Container(
              child: Container(
                height: 70.0,
                child: Stack(
                  children: [
                    Container(color: getBgcolorByStatus(menuItem.status)),
                    Row(
                      children: [
                        SizedBox(
                          width: 6,
                          height: 70,
                          child: Container(
                            color: leftMenuColor,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: SvgPicture.asset(
                              getNameByStatus(menuItem.status, index)),
                        ),
                        Text(
                          menuItem.name,
                          style: TextStyle(
                              fontSize: 20.0,
                              color: getTextColorByStatus(menuItem.status)),
                        ),
                      ],
                    ),
                    Visibility(
                        visible: menuItem.status == MenuItemStatus.DISABLED,
                        child: Container(color: MyColors.lockedGrayedBg)),
                    Row(
                      children: [
                        Spacer(),
                        Stack(children: [
                          Visibility(
                              child: Center(
                            child: Padding(
                              padding: const EdgeInsets.only(right: 12),
                              child: SvgPicture.asset(
                                  getImageByStatus(menuItem.status)),
                            ),
                          )),
                          Visibility(
                            visible: menuItem.status == MenuItemStatus.DISABLED,
                            child: Center(
                              child: Stack(
                                alignment: Alignment.center,
                                children: <Widget>[
                                  ClipRRect(
                                    borderRadius: BorderRadius.only(
                                        topLeft: const Radius.circular(4.0),
                                        bottomLeft: const Radius.circular(4.0)),
                                    child: Container(
                                      color: MyColors.lockedBg,
                                      width: 52,
                                      height: 44,
                                    ),
                                  ),
                                  SvgPicture.asset('images/ic_locked.svg')
                                ],
                              ),
                            ),
                          )
                        ])
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  void navigateToMainScreen(
      BuildContext context, MenuItem menuItem, int totalLevelCount) async {
    var menuNumber = menuItem.index;
    var menuName = menuItem.name;
    final String fileName = "$menuNumber.$menuName";
    var levelDataList = await FileReader().parseFile(fileName, menuNumber);
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => MainScreen(
                index: menuNumber,
                levelName: menuName,
                levelDataList: levelDataList,
                targetingInfo: widget.targetingInfo,
                progress: progress,
                saveProgressLevelState: () {
                  Future.delayed(
                      Duration.zero,
                      () => setState(() {
                            progress = SharedPreferenceManager.getProgress(
                                SharedPreferenceManager.CURRENT_PROGRESS_KEY);
                          }));
                },
                updateCoins: () {
                  Future.delayed(
                      Duration.zero,
                      () => setState(() {
                            progress = SharedPreferenceManager.getProgress(
                                SharedPreferenceManager.CURRENT_PROGRESS_KEY);
                          }));
                },
                categotyCompleted: () {},
              )),
    );
  }

  Widget createLoadingPlaceholder() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            child: CircularProgressIndicator(),
            width: 60,
            height: 60,
          ),
          const Padding(
            padding: EdgeInsets.only(top: 16),
            child: Text('Awaiting result...'),
          )
        ],
      ),
    );
  }

  getColorByStatus(MenuItemStatus status) {
    if (status == MenuItemStatus.ENABLED) {
      return MyColors.buttonLightPurple;
    } else if (status == MenuItemStatus.COMPLETED) {
      return MyColors.buttonBlue;
    } else {
      return null;
    }
  }

  Future<Progress> loadProgress() async {
    Progress progress = SharedPreferenceManager.getProgress(
        SharedPreferenceManager.CURRENT_PROGRESS_KEY);
    return await Future.value(progress);
  }

  List<MenuItem> mergeItemsWithProgress(
      List<MenuItem> items, Progress savedProgress) {
    for (int i = 0; i < items.length; i++) {
      MenuItem item = items[i];
      List<ProgressLevel> progressLevels = savedProgress.progressLevels;
      for (int j = 0; j < savedProgress.progressLevels.length; j++) {
        ProgressLevel progressLevel = progressLevels[j];
        if (progressLevel.menuItem == item.index - 1) {
          if (progressLevel.isCompleted) {
            item.status = MenuItemStatus.COMPLETED;
          } else {
            item.status = MenuItemStatus.ENABLED;
          }
        }
      }
    }
    return items;
  }

  getBgcolorByStatus(MenuItemStatus status) {
    if (status == MenuItemStatus.COMPLETED) {
      return MyColors.itemPaintCompleted;
    } else {
      return MyColors.itemPaint;
    }
  }

  String getNameByStatus(MenuItemStatus status, int index) {
    if (status == MenuItemStatus.COMPLETED) {
      return 'images/i${index + 1}c.svg';
    } else {
      return 'images/i${index + 1}.svg';
    }
  }

  String getImageByStatus(MenuItemStatus status) {
    if (status == MenuItemStatus.ENABLED) {
      return 'images/ic_available.svg';
    } else {
      return 'images/ic_completed.svg';
    }
  }

  getTextColorByStatus(MenuItemStatus status) {
    if (status == MenuItemStatus.COMPLETED) {
      return MyColors.dialogTextColor;
    } else {
      return null;
    }
  }

  void showLevelCompletedDialog() {
    DialogUtils.showCongratulationsDialog(
        context,
        'images/ic_upvote.svg',
        "Brilliant job",
        "You have done this category. Choose another one and enjoy the game",
        "Choose category",
        180,
        callback: () {});
  }

  void showLevelIsDisabledDIalog(int menuItemIndex) {
    int neededCoins = Coins.openLevel;
    int playCoins = SharedPreferenceManager.getCoins();
    DialogUtils.showBuyLevelDialog(
        context,
        'images/vector_info.svg',
        "Oh, no!",
        "Level is closed, but you can open it for $neededCoins coins",
        "Open level", callback: () {
      if (playCoins < neededCoins) {
        Future.delayed(const Duration(milliseconds: 250), () {
          if (isRewardedAdsLoaded) {
            showGetCoinsDialog("Not enough coins. Get more...");
          } else {
            Toast.show("Not enough coins", context);
          }
        });
      } else {
        ProgressLevel nextProgressLevel =
            ProgressLevel(menuItemIndex - 1, 0, 0, false, List());

        progress.updateProgressLevel(
            menuItemIndex - 1, List.of([nextProgressLevel]));

        SharedPreferenceManager.setProgress(progress);
        SharedPreferenceManager.setCoins(
            SharedPreferenceManager.getCoins() - neededCoins);

        setState(() {});
      }
    });
  }

  void showGetCoinsDialog(String dialogText) {
    DialogUtils.showGetCoinsDialog(context, dialogText, freeButtonClick: () async {
      await RewardedVideoAd.instance.show();
    });
  }

  void updateStateWithCoins(int coins) {
    setState(() {
      SharedPreferenceManager.setCoins(
          SharedPreferenceManager.getCoins() + coins);
    });
  }
}
