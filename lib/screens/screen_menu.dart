import 'dart:io';

import 'package:findwords/custom/icons/find_words_crossword_icons.dart';
import 'package:findwords/mycolors.dart';
import 'package:findwords/screens/ramdon_levels/screen_random_menu.dart';
import 'package:findwords/screens/screen_campaign_levels_list.dart';
import 'package:findwords/screens/screen_settings.dart';
import 'package:findwords/strings.dart';
import 'package:findwords/utils/advert_utils.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
  keywords: <String>['word puzzle', 'solve puzzle', 'puzzle'],
  contentUrl: 'https://flutter.io',
  childDirected: true,
  testDevices: <String>[], // Android emulators are considered test devices
);

BannerAd myBanner = BannerAd(
  adUnitId: AdvertUtils.getBannerUnitId(),
  size: AdSize.banner,
  targetingInfo: targetingInfo,
  listener: (MobileAdEvent event) {
    print("BannerAd event is $event");
  },
);

class MenuScreen extends StatefulWidget {
  MenuScreen({Key key}) : super(key: key);

  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  bool _isLoadVisible = false;

  @override
  void initState() {
    super.initState();
    FirebaseAdMob.instance.initialize(appId: AdvertUtils.getAppId());
    myBanner
      //typically this happens well before the ad is shown
      ..load()
      ..show(
        anchorType: AnchorType.bottom,
      );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        child: Center(
          child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [MyColors.bgColorTop, MyColors.bgColorBottom])),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                    child: Image(image: AssetImage("images/splash.png")),
                    height: 140,
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 24),
                        child: SizedBox(
                          width: 270,
                          height: 66,
                          child: RaisedButton(
                            onPressed: () {
                              if (!_isLoadVisible) {
                                setState(() {
                                  _isLoadVisible = true;
                                });
                                Future.delayed(
                                    const Duration(milliseconds: 200), () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            CampaignLevelsListScreen(
                                              targetingInfo: targetingInfo,
                                            )),
                                  );
                                });
                                Future.delayed(
                                    const Duration(milliseconds: 1000), () {
                                  setState(() {
                                    _isLoadVisible = false;
                                  });
                                });
                              }
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(6.0))),
                            child: Row(
                              children: [
                                Icon(
                                  FindWordsCrossword.crossword,
                                  color: Colors.white,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 20.0),
                                  child: Text(
                                    "Campaign",
                                    style: TextStyle(
                                        fontSize: 22.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey[800]),
                                  ),
                                )
                              ],
                            ),
                            textColor: Colors.white,
                            splashColor: Colors.red,
                            color: Colors.amber[400],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 24),
                        child: SizedBox(
                          width: 270,
                          height: 66,
                          child: RaisedButton(
                            onPressed: () {
                              if (!_isLoadVisible) {
                                setState(() {
                                  _isLoadVisible = true;
                                });
                                Future.delayed(
                                    const Duration(milliseconds: 200), () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => RandomScreenMenu(
                                              targetingInfo: targetingInfo,
                                            )),
                                  );
                                });
                                Future.delayed(
                                    const Duration(milliseconds: 1000), () {
                                  setState(() {
                                    _isLoadVisible = false;
                                  });
                                });
                              }
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(6.0))),
                            child: Row(
                              children: [
                                Icon(
                                  FindWordsCrossword.crossword,
                                  color: Colors.white,
                                ),
                                Padding(
                                    padding: EdgeInsets.only(left: 20.0),
                                    child: Text(
                                      Strings.menu_campaign_level,
                                      style: TextStyle(
                                          fontSize: 22.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey[800]),
                                    ))
                              ],
                            ),
                            textColor: Colors.white,
                            splashColor: Colors.red,
                            color: Colors.amber[400],
                          ),
                        ),
                      ),
                      Visibility(
                        visible: Platform.isAndroid,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: 80,
                              height: 66,
                              child: RaisedButton(
                                onPressed: () {
                                  Future.delayed(
                                      const Duration(milliseconds: 200), () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              SettingsScreen()),
                                    );
                                  });
                                },
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(6.0))),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      FindWordsCrossword.ic_setting,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                                textColor: Colors.white,
                                splashColor: Colors.red,
                                color: Colors.amber[400],
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(right: 10)),
                            SizedBox(
                              width: 180,
                              height: 66,
                              child: RaisedButton(
                                onPressed: () {
                                  SystemNavigator.pop();
                                },
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(6.0))),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.exit_to_app,
                                      color: Colors.white,
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(left: 20.0),
                                        child: Text(
                                          "Exit",
                                          style: TextStyle(
                                              fontSize: 22.0,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.grey[800]),
                                        ))
                                  ],
                                ),
                                textColor: Colors.white,
                                splashColor: Colors.red,
                                color: Colors.amber[400],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Visibility(
                        visible: Platform.isIOS,
                        child: SizedBox(
                          width: 270,
                          height: 66,
                          child: RaisedButton(
                            onPressed: () {
                              Future.delayed(const Duration(milliseconds: 200),
                                  () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SettingsScreen()),
                                );
                              });
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(6.0))),
                            child: Row(
                              children: [
                                Icon(
                                  FindWordsCrossword.ic_setting,
                                  color: Colors.white,
                                ),
                                Padding(
                                    padding: EdgeInsets.only(left: 20.0),
                                    child: Text(
                                      "Settings",
                                      style: TextStyle(
                                          fontSize: 22.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey[800]),
                                    ))
                              ],
                            ),
                            textColor: Colors.white,
                            splashColor: Colors.red,
                            color: Colors.amber[400],
                          ),
                        ),
                      ),
                      Visibility(
                          visible: _isLoadVisible,
                          maintainSize: true,
                          maintainAnimation: true,
                          maintainState: true,
                          child: Container(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 20.0),
                              child: Text(
                                "Loading...",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          )),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> testInit() async {
    /*final QueryPurchaseDetailsResponse response1 = await InAppPurchaseConnection.instance.queryPastPurchases();
    if (response1.error != null) {
      // Handle the error.
    }
    Toast.show(response1.pastPurchases.length.toString(), context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);*/
  }
}
