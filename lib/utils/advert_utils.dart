import 'dart:io';

import 'package:firebase_admob/firebase_admob.dart';

class AdvertUtils {
  static String getAppId() {
    if (Platform.isAndroid) {
      return "ca-app-pub-2204076909967153~7310677526";
    } else {
      return "ca-app-pub-2204076909967153~4830620310";
    }
    //return FirebaseAdMob.testAppId;
  }

  static String getBannerUnitId() {
    if (Platform.isAndroid) {
      return "ca-app-pub-2204076909967153/5764944957";
    } else {
      return "ca-app-pub-2204076909967153/3621480423";
    }
    //return BannerAd.testAdUnitId;
  }

  static String getRewardUnitId() {
    if (Platform.isAndroid) {
      return "ca-app-pub-2204076909967153/5322673352";
    } else {
      return "ca-app-pub-2204076909967153/6245050394";
    }
    //return RewardedVideoAd.testAdUnitId;
  }

  static void handleRewardFlow(MobileAdTargetingInfo targetingInfo,
      {Function(int) onRewardedSuccess, Function(bool) onLoaded}) {
    RewardedVideoAd.instance
        .load(
          adUnitId: getRewardUnitId(),
          targetingInfo: targetingInfo,
        )
        .catchError((e) => print("ERROR!!!! LOADING REWARDED FIRST TIME!"))
        .then((value) => onLoaded(value));
    RewardedVideoAd.instance.listener =
        (RewardedVideoAdEvent event, {String rewardType, int rewardAmount}) {
      if (event == RewardedVideoAdEvent.rewarded) {
        if (onRewardedSuccess != null) {
          onRewardedSuccess.call(rewardAmount);
        }
      } else if (event == RewardedVideoAdEvent.closed) {
        RewardedVideoAd.instance
            .load(
              adUnitId: getRewardUnitId(),
              targetingInfo: targetingInfo,
            )
            .catchError((e) => print("ERROR!!!! LOADING REWARDED AGAIN!"))
            .then((value) => onLoaded(value));
      }
    };
  }
}
