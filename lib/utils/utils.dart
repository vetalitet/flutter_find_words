class Utils {
  static final RegExp intRegex = RegExp(r'\d+');
  static final RegExp fromDotRegex = RegExp(r'\.(.+)');
  static final String baseFilePath = "assets/prod";
  static final String baseLevelWordsPath = "assets/groups";
  static final String baseLevelRoutesPath = "assets/routes";
  static final String popupAlias = "findWordsPopup";
  static final String passwordPhrase = "vetalitet_findwords";
}
