import 'dart:convert';

import 'package:findwords/data/menu_level.dart';
import 'package:findwords/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';

Future<SvgPicture> loadSvg(BuildContext context, String path) async {
  var picture = SvgPicture.asset(path);
  await precachePicture(picture.pictureProvider, context);
  return picture;
}

Future<List<MenuItem>> loadLevels(context, String path) async {
  final manifestJson =
      await DefaultAssetBundle.of(context).loadString('AssetManifest.json');
  final levelsFiles = json
      .decode(manifestJson)
      .keys
      .where((String key) => key.startsWith('$path/'));

  List<MenuItem> list = [];
  levelsFiles.forEach((element) {
    var index = Utils.intRegex.stringMatch(element).toString();
    var name = Utils.fromDotRegex.stringMatch(element).toString().split('.')[1];
    list.add(MenuItem(int.parse(index), name, MenuItemStatus.DISABLED));
  });
  list.sort((a, b) => a.index.compareTo(b.index));

  return list;
}
