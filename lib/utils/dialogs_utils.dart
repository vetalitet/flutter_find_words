import 'package:findwords/coins.dart';
import 'package:findwords/components/buttons/free_coins_button.dart';
import 'package:findwords/mycolors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class DialogUtils {
  static Future<bool> showCongratulationsDialog(
      BuildContext context,
      String image,
      String title,
      String description,
      String buttonText,
      int buttonWidth,
      {Function callback}) {
    var alertStyle = AlertStyle(
      isCloseButton: false,
      isOverlayTapDismiss: false,
      alertBorder: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(2.0),
        side: BorderSide(
          color: Colors.white,
        ),
      ),
      titleStyle: TextStyle(
          color: MyColors.dialogTextColor,
          fontSize: 22,
          fontWeight: FontWeight.bold),
      descStyle: TextStyle(color: MyColors.dialogTextColor, fontSize: 18),
    );
    return Alert(
        context: context,
        style: alertStyle,
        title: title,
        desc: description,
        image: SvgPicture.asset(image),
        buttons: [
          DialogButton(
            child: Text(
              buttonText,
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
            color: MyColors.buyCoinsButtonPurpleDark,
            onPressed: () {
              if (callback != null) callback();
              Navigator.pop(context);
            },
            width: buttonWidth.toDouble(),
            radius: BorderRadius.circular(2.0),
          )
        ]).show();
  }

  static Future<bool> showBuyLevelDialog(BuildContext context, String image,
      String title, String description, String positiveButtonLabel,
      {Function callback}) {
    var alertStyle = AlertStyle(
      isCloseButton: false,
      alertBorder: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(2.0),
        side: BorderSide(
          color: Colors.white,
        ),
      ),
      titleStyle: TextStyle(
          color: MyColors.dialogTextColor,
          fontSize: 22,
          fontWeight: FontWeight.bold),
      descStyle: TextStyle(color: MyColors.dialogTextColor, fontSize: 18),
    );
    return Alert(
        context: context,
        style: alertStyle,
        title: title,
        desc: description,
        image: SvgPicture.asset(image),
        buttons: [
          DialogButton(
            child: Text(
              "Cancel",
              style: TextStyle(color: MyColors.dialogTextColor, fontSize: 18),
            ),
            color: Colors.transparent,
            onPressed: () {
              Navigator.pop(context);
            },
            radius: BorderRadius.circular(2.0),
          ),
          DialogButton(
            child: Text(
              positiveButtonLabel,
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
            color: MyColors.buyCoinsButtonPurpleDark,
            onPressed: () {
              if (callback != null) callback();
              Navigator.pop(context);
            },
            radius: BorderRadius.circular(2.0),
          )
        ]).show();
  }

  static Future<void> showGetCoinsDialog(
    BuildContext context,
    String dialogText, {
    Function freeButtonClick,
  }) {
    //var isPaymentAllowed = purchaseBloc.products.length > 0;
    var isPaymentAllowed = false;
    return showDialog<bool>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.only(top: 10.0),
            content: Container(
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                borderRadius: new BorderRadius.all(Radius.circular(32.0)),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(22, 0, 22, 8),
                    child: Text(
                      dialogText,
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: MyColors.dialogTextColor),
                    ),
                  ),
                  Container(
                    height: 1,
                    color: MyColors.dialogDelimiterColor,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 16.0, 0, 0),
                        child: Text("Receive your coins", style: TextStyle(
                          fontSize: 18,
                          color: MyColors.buttonBlue,
                        )),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 16.0, 0, 16.0),
                        child: MaterialButton(
                          minWidth: 126,
                          height: 40,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(4.0),
                              side:
                                  BorderSide(color: MyColors.freeButtonColor)),
                          color: MyColors.freeButtonColor,
                          elevation: 0,
                          child: new Text('Free',
                              style: new TextStyle(
                                  fontSize: 18.0, color: Colors.white)),
                          onPressed: () {
                            if (freeButtonClick != null) {
                              freeButtonClick();
                              Navigator.pop(context);
                            }
                          },
                        ),
                      ),
                      Visibility(
                        visible: false,
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: SizedBox(
                            child: FreeCoinsButton(
                              coinsQuantity: Coins.lowReward,
                              buttonPrice: Coins.lowRewardPrice,
                              isEnabled: isPaymentAllowed,
                              onClick: () {
                                if (isPaymentAllowed) {
                                  //purchaseBloc.buyProduct(purchaseBloc.products[0]);
                                  Navigator.pop(context);
                                }
                              },
                            ),
                            width: 130,
                            height: 40,
                          ),
                        ),
                      ),
                      Visibility(
                        visible: false,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 4, 0, 18.0),
                          child: SizedBox(
                            child: FreeCoinsButton(
                              coinsQuantity: Coins.highReward,
                              buttonPrice: Coins.highRewardPrice,
                              isEnabled: isPaymentAllowed,
                              onClick: () {
                                if (isPaymentAllowed) {
                                  //purchaseBloc.buyProduct(purchaseBloc.products[1]);
                                  Navigator.pop(context);
                                }
                              },
                            ),
                            width: 130,
                            height: 40,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    color: MyColors.bottomAreaColor,
                    height: 52,
                    child: Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 8.0, 0),
                          child: RaisedButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            elevation: 0,
                            child: Text(
                              'Cancel',
                              style: TextStyle(
                                fontSize: 18,
                                color: MyColors.buttonBlue,
                              ),
                            ),
                          ),
                        )),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
