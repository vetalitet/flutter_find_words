import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

double getWordTableSize(BuildContext context, int cellsCount) {
  var width = MediaQuery.of(context).size.width;
  if (width < 550) {
    var additionalCells = 2;
    if (cellsCount > 5) {
      additionalCells = 1;
    }
    var oneCellWidth = width / (cellsCount + additionalCells);
    return oneCellWidth * cellsCount;
  } else if (width >= 550 && width < 700) {
    return 320.0;
  } else {
    return 400.0;
  }
}

double getArrowMargin(BuildContext context) {
  var width = MediaQuery.of(context).size.width;
  if (width < 550) {
    return 28.0;
  } else {
    return 20.0;
  }
}
