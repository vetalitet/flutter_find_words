class Coins {
  static final int initial = 150;
  static final int levelCompleted = 5;
  static final int categoryCompleted = 20;
  static final int openLevel = 50;
  static final int freeReward = 10;
  static final int lowReward = 250;
  static final int highReward = 600;
  static final int lowRewardPrice = 1;
  static final int highRewardPrice = 2;
  static final int lowHintCost = 15;
  static final int highHintCost = 30;
}
