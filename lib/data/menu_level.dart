class MenuItem {
  final int index;
  final String name;
  MenuItemStatus status;

  MenuItem(this.index, this.name, this.status);
}

enum MenuItemStatus { DISABLED, ENABLED, COMPLETED }
