import 'package:findwords/data/enums/rect_status.dart';
import 'package:flutter/widgets.dart';

class RectItem {
  Rect rect;
  String char;
  int defaultColor;
  int x;
  int y;

  RectStatus status = RectStatus.NONE;
  int index = 0;
  int color;

  RectItem(
    Rect rect,
    String char,
    int defaultColor,
    int x,
    int y,
  ) {
    this.rect = rect;
    this.char = char;
    this.defaultColor = defaultColor;
    this.x = x;
    this.y = y;

    color = defaultColor;
  }

  void clearItem() {
    status = RectStatus.NONE;
    index = 0;
    color = defaultColor;
  }
}
