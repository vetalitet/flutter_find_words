class XYChar {
  int x;
  int y;
  String char;

  XYChar(int x, int y, String char) {
    this.x = x;
    this.y = y;
    this.char = char;
  }
}
