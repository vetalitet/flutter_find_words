import 'dart:collection';

class Progress {
  String progressId;
  int wordsCount;
  List<ProgressLevel> progressLevels;

  Progress(this.progressId, this.wordsCount, [this.progressLevels]);

  void updateProgressLevel(
      int menuItem, List<ProgressLevel> changedProgressLevels) {
    List<ProgressLevel> updatedList = List();

    Set<int> indexes = HashSet();

    for (int i = 0; i < changedProgressLevels.length; i++) {
      ProgressLevel changedProgressLevel = changedProgressLevels[i];
      ProgressLevel appropriateProgressLevel =
          findAppropriateProgressLevel(changedProgressLevel);
      if (appropriateProgressLevel != null) {
        updatedList.add(appropriateProgressLevel);
        indexes.add(appropriateProgressLevel.menuItem);
      } else {
        updatedList.add(changedProgressLevel);
        indexes.add(changedProgressLevel.menuItem);
      }
    }

    progressLevels.forEach((element) {
      if (!indexes.contains(element.menuItem)) {
        updatedList.add(element);
      }
    });

    updatedList.sort((a, b) => a.menuItem.compareTo(b.menuItem));

    progressLevels = updatedList;
  }

  ProgressLevel findProgressLevelById(int index) {
    ProgressLevel result;
    for (int i = 0; i < progressLevels.length; i++) {
      final ProgressLevel progressLevel = progressLevels[i];
      if (progressLevel.menuItem == index) {
        result = progressLevel;
      }
    }
    return result;
  }

  ProgressLevel findAppropriateProgressLevel(
      ProgressLevel changedProgressLevel) {
    ProgressLevel result;
    for (int i = 0; i < progressLevels.length; i++) {
      ProgressLevel currentProgressLevel = progressLevels[i];
      if (currentProgressLevel.menuItem == changedProgressLevel.menuItem) {
        result = currentProgressLevel;
      }
    }
    return result;
  }

  Map toJson() {
    List<Map> progressLevels = this.progressLevels != null
        ? this.progressLevels.map((i) => i.toJson()).toList()
        : null;
    return {
      'progressId': progressId,
      'wordsCount': wordsCount,
      'progressLevels': progressLevels
    };
  }

  factory Progress.fromJson(dynamic json) {
    if (json['progressLevels'] != null) {
      var pointsObjsJson = json['progressLevels'] as List;
      List<ProgressLevel> points = pointsObjsJson
          .map((tagJson) => ProgressLevel.fromJson(tagJson))
          .toList();
      return Progress(json['progressId'], json['wordsCount'] as int, points);
    } else {
      return Progress(json['progressId'], json['wordsCount'] as int);
    }
  }
}

class ProgressLevel {
  int menuItem;
  int currentSublevel;
  int foundWordsCount;
  bool isCompleted;
  List<ProgressPoints> progressPoints;

  ProgressLevel(this.menuItem, this.currentSublevel, this.foundWordsCount,
      this.isCompleted,
      [this.progressPoints]);

  Map toJson() {
    List<Map> progressPoints = this.progressPoints != null
        ? this.progressPoints.map((i) => i.toJson()).toList()
        : null;
    return {
      'menuItemIndex': menuItem,
      'currentSublevel': currentSublevel,
      'foundWordsCount': foundWordsCount,
      'isCompleted': isCompleted,
      'progressPoints': progressPoints == null ? List() : progressPoints
    };
  }

  factory ProgressLevel.fromJson(dynamic json) {
    if (json['progressPoints'] != null) {
      var pointsObjsJson = json['progressPoints'] as List;
      List<ProgressPoints> points = pointsObjsJson
          .map((tagJson) => ProgressPoints.fromJson(tagJson))
          .toList();
      return ProgressLevel(
          json['menuItemIndex'] as int,
          json['currentSublevel'] as int,
          json['foundWordsCount'] as int,
          json['isCompleted'] as bool,
          points);
    } else {
      return ProgressLevel(
          json['menuItemIndex'] as int,
          json['currentSublevel'] as int,
          json['foundWordsCount'] as int,
          json['isCompleted'] as bool);
    }
  }

  int containsElement(int x, int y) {
    if (progressPoints != null) {
      for (int i = 0; i < progressPoints.length; i++) {
        var elem = progressPoints[i];
        if (elem.x == x && elem.y == y) {
          return elem.color;
        }
      }
    }
    return 0;
  }
}

class ProgressPoints {
  int x;
  int y;
  int color;
  ProgressPoints(this.x, this.y, this.color);

  Map toJson() => {'x': x, 'y': y, 'color': color};

  factory ProgressPoints.fromJson(Map<String, dynamic> json) {
    return ProgressPoints(json['x'], json['y'], json['color']);
  }
}
