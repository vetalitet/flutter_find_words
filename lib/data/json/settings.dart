class Settings {
  bool isHintShown;
  bool isAdditionalInfoShown;
  Settings(this.isHintShown, this.isAdditionalInfoShown);

  Map toJson() {
    return {
      'isHintShown': isHintShown,
      'isAdditionalInfoShown': isAdditionalInfoShown
    };
  }

  factory Settings.fromJson(dynamic json) {
    return Settings(json['isHintShown'], json['isAdditionalInfoShown']);
  }
}
