import 'package:findwords/data/chart_size_data.dart';
import 'package:findwords/data/enums/equals_enum.dart';
import 'package:findwords/data/word_data.dart';
import 'package:findwords/data/xy_char.dart';
import 'package:tuple/tuple.dart';

class LevelData {
  ChartSizeData chartSizeData;
  int levelNumber;
  int menuItemNumber;
  List<WordData> words;

  LevelData(
    ChartSizeData chartSizeData,
    int levelNumber,
    int menuItemNumber,
    List<WordData> words,
  ) {
    this.chartSizeData = chartSizeData;
    this.levelNumber = levelNumber;
    this.menuItemNumber = menuItemNumber;
    this.words = words;
  }

  String getCharBy(int x, int y) {
    String char;
    for (final WordData word in words) {
      word.items
          .where((element) => element.x == x && element.y == y)
          .forEach((element) {
        char = element.char;
      });
    }
    return char;
  }

  int getStatusBy(int x, int y) {
    int status;
    for (final WordData word in words) {
      word.items
          .where((element) => element.x == x && element.y == y)
          .forEach((element) {
        status = element.status;
      });
    }
    return status;
  }

  int getColorBy(int x, int y) {
    int color = 0;
    for (final WordData word in words) {
      word.items
          .where((element) => element.x == x && element.y == y)
          .forEach((element) {
        color = element.color;
      });
    }
    return color;
  }

  Tuple2<int, EqualsEnum> checkContainsWord(List<XYChar> selectedWord) {
    Tuple2<int, EqualsEnum> result = Tuple2(0, EqualsEnum.NOT_EQUALS);
    words.forEach((element) {
      var wordString = new StringBuffer();
      var selectedString = new StringBuffer();
      element.items.forEach((element) {
        wordString.write(element.char);
      });
      selectedWord.forEach((element) {
        selectedString.write(element.char);
      });
      if (wordString.toString().toUpperCase() ==
          selectedString.toString().toUpperCase()) {
        for (int i = 0; i <= element.items.length - 1; i++) {
          var item = element.items[i];
          var value = selectedWord[i];
          if (item.x != value.x || item.y != value.y) {
            result = Tuple2(0, EqualsEnum.EQUALS_BUT_WRONG_POSITIONS);
            break;
          }
        }
        if (result.item2 == EqualsEnum.NOT_EQUALS) {
          result =
              Tuple2(element.items[0].wordNumber, EqualsEnum.EQUALS_COMPLETELY);
        }
      }
    });
    return result;
  }
}
