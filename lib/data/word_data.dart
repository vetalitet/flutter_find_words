import 'package:findwords/data/item_data.dart';

class WordData {
  List<ItemData> items;

  WordData(List<ItemData> items) {
    this.items = items;
  }
}
