class ItemData {
  int x;
  int y;
  int charNumber;
  int wordNumber;
  int status;
  int color;
  String char;

  ItemData(
    int x,
    int y,
    int charNumber,
    int wordNumber,
    int status,
    int color,
    String char,
  ) {
    this.x = x;
    this.y = y;
    this.charNumber = charNumber;
    this.wordNumber = wordNumber;
    this.status = status;
    this.color = color;
    this.char = char;
  }
}
