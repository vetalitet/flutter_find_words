import 'package:findwords/data/chart_size_data.dart';
import 'package:findwords/data/enums/rect_status.dart';
import 'package:findwords/data/item_data.dart';
import 'package:findwords/data/word_data.dart';
import 'package:findwords/data/xy_char.dart';
import 'package:findwords/utils/encrypt_utils.dart';
import 'package:findwords/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:findwords/data/level_data.dart';

class FileReader {
  final chart_size_regexp = RegExp(r'(\dx\d)');
  final item_regex = RegExp(r'(\d/\d/[a-zA-Z])');

  Future<List<LevelData>> parseFile(String fileName, int menuItemNumber) async {
    final fileContent =
        await rootBundle.loadString('${Utils.baseFilePath}/$fileName');
    String decryptedString =
        EncryptUtils.decryptAES(fileContent, Utils.passwordPhrase);
    final List<String> lines = decryptedString.split("\n");

    int width = 0;
    int height = 0;
    var levelId = 1;
    var readNextNLines = 0;

    var charNumber = 0;
    var wordNumber = 1;

    List<LevelData> levels = List();
    List<WordData> words = List();
    List<ItemData> items = List();

    for (var i = 0; i < lines.length; i++) {
      var line = lines[i].trim();
      if (line.startsWith("//")) {
        continue;
      }
      var result = chart_size_regexp.stringMatch(line);
      if (result != null) {
        var size = result.split("x");
        width = int.parse(size[0]);
        height = int.parse(size[1]);
        readNextNLines = width * height;
      } else {
        if (line.isNotEmpty) {
          var result = item_regex.stringMatch(line);
          var posChar = result.split("/");
          var x = int.parse(posChar[0]);
          var y = int.parse(posChar[1]);
          var char = posChar[2];
          items.add(ItemData(
              x, y, charNumber, wordNumber, RectStatus.NONE.index, 0, char));
          charNumber++;
          readNextNLines--;
          if (readNextNLines == 0) {
            if (items.isNotEmpty) {
              words.add(WordData(items));
              items = List();
              wordNumber++;
            }
          }
        } else {
          if (items.isNotEmpty) {
            words.add(WordData(items));
            items = List();
            wordNumber++;
          }
        }
      }
      if (words.isNotEmpty && readNextNLines == 0) {
        levels.add(LevelData(
            ChartSizeData(width, height), levelId, menuItemNumber, words));
        words = List();
        levelId++;
        wordNumber = 1;
      }
    }

    return levels;
  }

  Future<Map<int, List<String>>> parseLevelWords(String fileName) async {
    final fileContent = await rootBundle.loadString(fileName);
    String decryptedString =
        EncryptUtils.decryptAES(fileContent, Utils.passwordPhrase);
    final List<String> lines = decryptedString.split("\n");

    final Map<int, List<String>> resultMap = Map();

    int wordLength = -1;
    List<String> items = List();
    for (var i = 0; i < lines.length; i++) {
      var line = lines[i].trim();
      int lineLength = line.length;
      if (!line.startsWith("//")) {
        if (wordLength == -1 || wordLength == lineLength) {
          items.add(line);
        } else {
          resultMap[wordLength] = items;
          items = List();
          items.add(line);
        }
        wordLength = lineLength;
      }
    }
    resultMap[wordLength] = items;

    return resultMap;
  }

  Future<List<RandomLevel>> parseLevelRoutes(String fileName) async {
    final fileContent = await rootBundle.loadString(fileName);
    String decryptedString =
        EncryptUtils.decryptAES(fileContent, Utils.passwordPhrase);
    final List<String> lines = decryptedString.split("\n");
    final List<RandomLevel> randomLevels = List();
    for (var i = 0; i < lines.length; i++) {
      var line = lines[i];
      List<String> words = line.split("-");
      List<RandomWord> randomWords = List();
      for (var j = 0; j < words.length; j++) {
        var word = words[j];
        List<String> points = word.split(",");
        List<XYChar> xychars = List();
        for (var k = 0; k < points.length; k++) {
          var point = points[k];
          List<String> positions = point.split("\.");
          XYChar xychar =
              XYChar(int.parse(positions[0]), int.parse(positions[1]), "");
          xychars.add(xychar);
        }
        randomWords.add(RandomWord(points: xychars));
      }
      randomLevels.add(RandomLevel(words: randomWords));
    }
    return randomLevels;
  }
}

class RandomWord {
  final List<XYChar> points;
  RandomWord({@required this.points});
}

class RandomLevel {
  final List<RandomWord> words;
  RandomLevel({@required this.words});
}
