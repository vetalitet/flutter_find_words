import 'dart:math';

import 'package:findwords/data/rect_item.dart';
import 'package:tuple/tuple.dart';

class ItemSizeRectItems {
  double itemSize;
  List<RectItem> rectItems;
  List<Tuple2<Point, Point>> horizontalPoints;
  List<Tuple2<Point, Point>> verticalPoints;

  ItemSizeRectItems(
      double itemSize,
      List<RectItem> rectItems,
      List<Tuple2<Point, Point>> horizontalPoints,
      List<Tuple2<Point, Point>> verticalPoints) {
    this.itemSize = itemSize;
    this.rectItems = rectItems;
    this.horizontalPoints = horizontalPoints;
    this.verticalPoints = verticalPoints;
  }
}
