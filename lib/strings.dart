class Strings {
  static final String hmm = "Hmm!";
  static final String words_exists =
      "This word exists. But try\n to make it up in another way";
  static final String ok_lets_try = "Ok, let's try";
  static final String menu_campaign_level = "Custom puzzle";
}
