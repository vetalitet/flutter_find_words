// @dart=2.9

import 'dart:async';
import 'dart:typed_data';
import 'dart:ui';

import 'package:findwords/screens/screen_splash.dart';
import 'package:findwords/shared_preferences/shared_preferences_manager.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//import 'package:in_app_purchase/in_app_purchase.dart';

import 'mycolors.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //InAppPurchaseConnection.enablePendingPurchases();
  await Firebase.initializeApp();
  await SharedPreferenceManager.getInstance();
  await loadImage("images/splash.png");
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    //purchaseBloc.init();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: MyColors.menuHeader,
    ));
    return MaterialApp(
      theme: ThemeData(
        textTheme: Theme.of(context).textTheme.apply(fontFamily: 'Muli'),
      ),
      home: Scaffold(
        appBar: null,
        body: SplashScreen(),
      ),
      builder: (BuildContext context, Widget child) {
        return Padding(
          padding: const EdgeInsets.only(bottom: 50),
          child: child,
        );
      },
    );
  }
}

Future<Uint8List> loadImage(String url) {
  ImageStreamListener listener;

  final Completer<Uint8List> completer = Completer<Uint8List>();
  final ImageStream imageStream =
      AssetImage(url).resolve(ImageConfiguration.empty);

  listener = ImageStreamListener(
    (ImageInfo imageInfo, bool synchronousCall) {
      imageInfo.image
          .toByteData(format: ImageByteFormat.png)
          .then((ByteData byteData) {
        imageStream.removeListener(listener);
        completer.complete(byteData.buffer.asUint8List());
      });
    },
    onError: (dynamic exception, StackTrace stackTrace) {
      imageStream.removeListener(listener);
      completer.completeError(exception);
    },
  );

  imageStream.addListener(listener);

  return completer.future;
}
