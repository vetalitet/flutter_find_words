import 'dart:convert';

import 'package:findwords/coins.dart';
import 'package:findwords/data/json/progress.dart';
import 'package:findwords/data/json/settings.dart';
import 'package:findwords/utils/encrypt_utils.dart';
import 'package:findwords/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceManager {
  static SharedPreferenceManager _shPM;
  static SharedPreferences _preferences;

  static Future getInstance() async {
    if (_shPM == null) {
      // keep local instance till it is fully initialized.
      var secureStorage = SharedPreferenceManager._();
      await secureStorage._init();
      _shPM = secureStorage;
    }
    return _shPM;
  }

  SharedPreferenceManager._();

  Future _init() async {
    _preferences = await SharedPreferences.getInstance();
  }

  static const String COINS_KEY = "CUB8H3D";
  static const String CURRENT_PROGRESS_KEY = "ZZZNHDPPR";
  static const String RANDOM_PROGRESS_KEY = "PPODNNCHD";

  static Progress getProgress(String key) {
    if (_preferences == null) return null;
    final String json = _preferences.getString(key);
    if (json == null) {
      ProgressLevel progressLevel = ProgressLevel(0, 0, 0, false);
      return Progress(
          key, 0, List<ProgressLevel>.generate(1, (i) => progressLevel));
    } else {
      try {
        var decrJson = EncryptUtils.decryptAES(json, Utils.popupAlias);
        var map = jsonDecode(decrJson);
        final Progress progress = Progress.fromJson(map);
        return progress;
      } catch (error) {
        ProgressLevel progressLevel = ProgressLevel(0, 0, 0, false);
        return Progress(
            key, 0, List<ProgressLevel>.generate(1, (i) => progressLevel));
      }
    }
  }

  static Future setProgress(Progress progress) async {
    if (_preferences == null) return null;
    final String json = jsonEncode(progress);
    var encrJson = EncryptUtils.encryptAES(json, Utils.popupAlias);
    return _preferences.setString(progress.progressId, encrJson);
  }

  static Future deleteProgress(String key) {
    if (_preferences == null) return null;
    return _preferences.remove(key);
  }

  static int getCoins() {
    if (_preferences == null) return null;
    final String coins = _preferences.getString(COINS_KEY);
    if (coins == null) {
      return Coins.initial;
    }
    var decrCoins = EncryptUtils.decryptAES(coins, Utils.popupAlias);
    return int.parse(decrCoins);
  }

  static Future setCoins(int coins) async {
    if (_preferences == null) return null;
    final String value = coins.toString();
    var encrCoins = EncryptUtils.encryptAES(value, Utils.popupAlias);
    return _preferences.setString(COINS_KEY, encrCoins);
  }

  static Settings getSettings() {
    if (_preferences == null) return null;
    final String json = _preferences.getString("settings");
    if (json == null) {
      return Settings(false, true);
    } else {
      var map = jsonDecode(json);
      return Settings.fromJson(map);
    }
  }

  static Future setSettings(Settings settings) async {
    if (_preferences == null) return null;
    final String json = jsonEncode(settings);
    return _preferences.setString("settings", json);
  }
}
